package app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Random;

/**
 * IL PING SERVER è essenzialmente un echo server: 

    rimanda al mittente qualsiasi dato riceve
    accetta un argomento da linea di comando: la porta, che è quella 
    su cui è attivo il server + un argomento opzionale, il seed, un valore 
    long utilizzato per la generazione di latenze e perdita di pacchetti. 
    Se uno qualunque degli argomenti è scorretto, stampa un messaggio 
    di errore del tipo ERR -arg x, dove x è il numero dell'argomento.
    dopo aver ricevuto un PING, il server determina se ignorare 
    il pacchetto (simulandone la perdita) o effettuarne l'eco. 
    La probabilità di perdita di pacchetti di default è del 25%.
    se decide di effettuare l'eco del PING, il server attende un 
    intervallo di tempo  casuale per simulare la latenza di rete
    stampa l'indirizzo IP e la porta del client, il messaggio di PING 
    e l'azione intrapresa dal server in seguito alla sua ricezione 
    (PING non inviato,oppure PING ritardato di x ms).
 */
public class MainClassServer {
    private static final int DELAY = 1000;

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("ERR - arg 1");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        int seed = 0;
        if (args.length == 2) {
            seed = Integer.parseInt(args[1]);
        }

        DatagramSocket serverSocket = new DatagramSocket(port);
        byte[] sendBuffer = new byte[1024];
        byte[] receivedBuffer = new byte[1024];
        // ricevero dei pacchetti di lunghezza massima 1024 contenuti in buffer
        DatagramPacket receivedPacket = new DatagramPacket(receivedBuffer, receivedBuffer.length);
        Random random = new Random();
        random.setSeed(seed);
        while (true) {
            try {
                serverSocket.receive(receivedPacket);
                String stringReceived = new String(receivedPacket.getData(), 0, receivedPacket.getLength(), "US-ASCII");
                if (random.nextDouble() < 0.25)
                    System.out.println(receivedPacket.getAddress().getHostAddress() + ":" + receivedPacket.getPort()
                            + "=> " + stringReceived + " LOST");
                else {
                    Thread.sleep((int) (random.nextDouble() * DELAY));
                    sendBuffer = stringReceived.getBytes();

                    DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length,
                            receivedPacket.getAddress(), receivedPacket.getPort());
                    serverSocket.send(sendPacket);
                    System.out.println(receivedPacket.getAddress().getHostAddress() + ":" + receivedPacket.getPort()
                            + "> " + stringReceived + " ACTION: delayed " + (int) (random.nextDouble() * DELAY)
                            + " ms");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}