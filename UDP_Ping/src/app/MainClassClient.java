package app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;

/**
 * Il PING CLIENT accetta due argomenti da linea di comando: nome e porta del server. 

    se uno o più argomenti risultano scorretti, il client termina, dopo aver stampato 
    un messaggio di errore del tipo ERR -arg x, dove x è il numero dell'argomento.
    utilizza una comunicazione UDP per comunicare con il server ed invia 10 messaggi 
    al server, con il seguente formato: PING seqno timestamp, in cui seqno è il 
    numero di sequenza del PING (tra 0-9) ed il timestamp (in millisecondi) indica quando 
    il messaggio è stato inviato
    non invia un nuovo PING fino che non ha ricevuto l'eco del PING precedente, 
    oppure è scaduto un timeout.
    stampa ogni messaggio spedito al server ed il RTT del ping oppure un * se la risposta 
    non è stata ricevuta entro 2 secondi
    dopo che ha ricevuto la decima risposta (o dopo il suo timeout), 
    il client stampa un riassunto simile a quello stampato dal PING UNIX

                        ---- PING Statistics ----

    10 packets transmitted, 7 packets received, 30% packet los

        round-trip (ms) min/avg/max = 63/190.29/290

        il RTT medio è stampato con 2 cifre dopo la virgola
 */
class MainClassClient {
    private final static int SIZE = 1024;
    private final static int PACKET_NUM = 10;;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("ERR - arg 1");
            System.exit(1);
        }
        if (args.length < 2) {
            System.err.println("ERR - arg 2");
            System.exit(2);
        }
        String host = args[0];
        int port = Integer.parseInt(args[1]);

        try {
            DatagramSocket clientSocket = new DatagramSocket();
            // creo l arappresentazione dell'indirizzo ip
            InetAddress ip = InetAddress.getByName(host);

            byte[] sendData = new byte[SIZE];
            byte[] receiveData = new byte[SIZE];

            int tot = 0, cont = 0, max = 0, avg = 0, min = 2001;
            for (int i = 0; i < PACKET_NUM; i++) {
                long timestamp = (new Date()).getTime();
                String ping = "PING " + i + " " + timestamp;
                sendData = ping.getBytes();
                // mando il messaggio i-esimo
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ip, port);
                clientSocket.send(sendPacket);
                tot++;
                try {
                    // setto il timeout  a massimo 2 secondi
                    clientSocket.setSoTimeout(2000);
                    // se il pacchetto è ricevuto
                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    clientSocket.receive(receivePacket);
                    String response = new String(receivePacket.getData(), 0, receivePacket.getLength(), "US-ASCII");
                    int rtt = (int) (new Date().getTime() - timestamp);
                    System.out.println(response + " RTT: " + rtt);
                    if (rtt > max)
                        max = rtt;
                    if (rtt < min)
                        min = rtt;
                    avg += rtt;
                    cont++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            clientSocket.close();
            System.out.println("---- PING Statistics ----");
            System.out.println(tot + " packets transmitted, " + cont + " packets received, " + (100 - cont * 100 / tot)
                    + "% packet loss");
            System.out.println("round-trip (ms) min/avg/max = " + min + "/" + String.format("%.02f", (float) avg / cont)
                    + "/" + max);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}