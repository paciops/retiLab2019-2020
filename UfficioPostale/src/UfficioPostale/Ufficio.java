package UfficioPostale;

import java.util.concurrent.ArrayBlockingQueue;

public class Ufficio {

	public static void main(String[] args)
			throws InterruptedException {
		int nThreads = 4, capacity = 10, dim = 100;
		Sportello sportelli = new Sportello(nThreads, capacity);
		ArrayBlockingQueue<Pensionato> utenti = new ArrayBlockingQueue<Pensionato>(
				dim);
		for (int i = 0; i < dim; i++) {
			utenti.add(new Pensionato("" + (i + 1)));
		}
		while (!utenti.isEmpty()) {
			if (sportelli.isAvailable())
				sportelli.execute(utenti.take());
		}
		sportelli.shutdown();
		if (sportelli.awaitTermination())
			System.out.println("È l'una, le poste sono chiuse");
		else {
			System.out.println("Le poste sono implose");
			System.exit(-1);
		}
	}

}
