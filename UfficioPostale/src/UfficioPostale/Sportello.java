package UfficioPostale;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Sportello {
	private ThreadPoolExecutor executor;
	private ArrayBlockingQueue<Runnable> list;
	private int capacity;
	public Sportello(int nThreads, int capacity) {
		this.capacity = capacity;
		this.list = new ArrayBlockingQueue<Runnable>(capacity);
		this.executor = new ThreadPoolExecutor(nThreads, nThreads,
				60L, TimeUnit.MILLISECONDS, list);
	}

	public void execute(Pensionato pensionato) {
		System.out.println(
				"Sportello: Qualcuno è venuto a ritirare la pensione");

		executor.execute(pensionato);

		System.out.printf(
				"Sportello: Stanno ancora ritirando la pensione in %d\n",
				executor.getActiveCount());
		System.out.printf(
				"Sportello: Hanno ritirato la pensione in %d\n",
				executor.getCompletedTaskCount());
	}

	public boolean isAvailable() {
		return list.size() < capacity;
	}

	public void shutdown() {
		executor.shutdown();
	}

	public boolean awaitTermination() {
		try {
			return executor.awaitTermination(10L, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}
}
