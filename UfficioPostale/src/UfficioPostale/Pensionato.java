package UfficioPostale;

public class Pensionato implements Runnable {
	private String name;

	public Pensionato(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		final String threadName = Thread.currentThread().getName();
		System.out.printf("%s: Pensionato %s\n", threadName, name);
		try {
			int max = 10000;
			Long duration = (long) (Math.random() * max);
			System.out.printf(
					"%s: Pensionato %s: Perde tempo per %d millisecondi\n",
					threadName, name, duration);
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("%s: Pensionato %s va al bar\n", threadName,
				name);
	}

}
