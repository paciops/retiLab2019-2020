abstract class Patient implements Runnable {
	protected long time;
	private static final int MAX = 500;
	private static final int MIN = 100;
	private String codice;

	public Patient(int examinations, String codice, int priority) {
		checkNumber(examinations);
		this.examinations = examinations;
		this.codice = codice;
		time = (long) (Math.random() * MAX) + MIN;
		Thread.currentThread().setPriority(priority);
	}

	private int examinations;

	public int getExaminations() {
		return examinations;
	}

	public void decrementExaminations() {
		checkNumber(examinations - 1);
		examinations--;
	}

	private void checkNumber(int n) {
		if (n < 0)
			throw new NumberFormatException();
	}

	@Override
	public void run() {
		while (getExaminations() != 0) {
			try {
				continueThread();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-2);
			} finally {
				decrementExaminations();
				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.exit(-2);
				}
			}
		}
		conclude();
	}

	@Override
	public String toString() {
		String name = Thread.currentThread().getName();
		return name + "\t" + codice + "\t exams: " + getExaminations();
	}

	public void print() {
		System.out.println(toString() + "\ttime: " + time + " ms\t");
	}

	abstract void continueThread() throws InterruptedException;

	abstract void conclude();
}

class RedCode extends Patient {
	private Doctor[] doctors;
	private Department department;

	public RedCode(int examinations, Doctor[] doctors, Department department) {
		super(examinations, "red", Thread.MAX_PRIORITY);
		this.doctors = doctors;
		this.department = department;
	}

	@Override
	void continueThread() throws InterruptedException {
		for (Doctor doc : doctors)
			doc.take();

		print();
		Thread.sleep(time);

		for (Doctor doc : doctors)
			doc.leave();
	}

	@Override
	void conclude() {
		department.redDecrement();
	}
}

class YellowCode extends Patient {
	private Doctor myDoc;
	private Department department;

	public YellowCode(int examinations, Doctor[] doctors, Department department) {
		super(examinations, "yellow", Thread.NORM_PRIORITY);
		int index = (int) Math.floor(Math.random() * doctors.length);
		myDoc = doctors[index];
		this.department = department;
	}

	@Override
	void continueThread() throws InterruptedException {
		department.waitReds();
		myDoc.take();
		print();
		Thread.sleep(time);
		myDoc.leave();
	}

	@Override
	void conclude() {
		department.yellowDecrement();
	}
}

class WhiteCode extends Patient {
	private Doctor[] doctors;
	private Department department;

	public WhiteCode(int examinations, Doctor[] doctors, Department department) {
		super(examinations, "white", Thread.MIN_PRIORITY);
		this.doctors = doctors;
		this.department = department;
	}

	@Override
	void continueThread() throws InterruptedException {
		department.waitYellows();
		boolean notVisited = true;
		while (notVisited) {
			for (Doctor doc : doctors) {
				if (doc.isFree() && notVisited) {
					doc.take();
					print();
					doc.leave();
					notVisited = false;
					break;
				}
			}
		}
	}

	@Override
	void conclude() {
	}
}