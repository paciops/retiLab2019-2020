import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Department {
	private final ReentrantLock redLock;
	private final ReentrantLock yellowLock;
	private final Condition redCondition;
	private final Condition yellowCondition;
	private int redCount;
	private int yellowCount;

	public Department(int reds, int yellows) {
		super();
		this.redCount = reds;
		this.yellowCount = yellows;
		this.redLock = new ReentrantLock();
		this.yellowLock = new ReentrantLock();
		this.redCondition = redLock.newCondition();
		this.yellowCondition = yellowLock.newCondition();
	}

	public void redDecrement() {
		redLock.lock();
		redCount--;
		if (redCount < 0)
			throw new IllegalStateException();
		if (redCount == 0)
			redCondition.signalAll();
		redLock.unlock();
	}

	public void yellowDecrement() {
		yellowLock.lock();
		yellowCount--;
		if (yellowCount == 0)
			yellowCondition.signalAll();
		yellowLock.unlock();
	}

	public void waitReds() throws InterruptedException {
		redLock.lock();
		try {
			while (redCount != 0)
				redCondition.await();
		} finally {
			redLock.unlock();
		}
	}

	public void waitYellows() throws InterruptedException {
		yellowLock.lock();
		try {
			while (yellowCount != 0)
				yellowCondition.await();
		} finally {
			yellowLock.unlock();
		}
	}
}
