import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class Hospital {
	private static final int MAX = 20;
	private static final int MIN = 1;

	private static Integer rand() {
		Double num = (Math.random() * MAX) + MIN;
		return num.intValue();
	}

	private static int[] initNumbers(String[] args) {
		int W = rand();
		int Y = rand();
		int R = rand();
		try {
			W = Integer.parseInt(args[0]);
			Y = Integer.parseInt(args[1]);
			R = Integer.parseInt(args[2]);
		} catch (Exception e) {
			System.err.println("Uso le variabili random");
			int[] res = { W, Y, R };
			return res;
		}
		int[] res = { W, Y, R };
		return res;
	}

	private static void randomInsert(ArrayList<Thread> list, int number, Class<? extends Patient> clazz,
			Doctor[] doctors, Department department) {
		while (number > 0) {
			int index = (int) Math.floor(Math.random() * list.size());
			int examinations = rand();
			try {
				Patient t;
				t = clazz.getDeclaredConstructor(int.class, Doctor.class.arrayType(), Department.class)
						.newInstance(examinations, doctors, department);
				list.add(index, new Thread(t));
				number--;
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}

	private static ArrayList<Thread> initPatients(int white, int yellow, int red, Doctor[] doctors,
			Department department) {
		ArrayList<Thread> list = new ArrayList<>();
		randomInsert(list, red, RedCode.class, doctors, department);
		randomInsert(list, yellow, YellowCode.class, doctors, department);
		randomInsert(list, white, WhiteCode.class, doctors, department);
		return list;
	}

	public static void main(String[] args) {
		int[] result = initNumbers(args);
		int W = result[0], Y = result[1], R = result[2];
		System.out.printf("%d - %d - %d\n", W, Y, R);
		Doctor[] doctors = new Doctor[10];
		Department department = new Department(R, Y);
		for (int i = 0; i < doctors.length; i++) {
			doctors[i] = new Doctor(i);
		}
		ArrayList<Thread> patients = initPatients(W, Y, R, doctors, department);
		patients.forEach(paz -> paz.start());
		patients.forEach(paz -> {
			try {
				paz.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		});
		System.out.println("Ortopedia chiusa");
	}

}
