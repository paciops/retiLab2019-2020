import java.util.concurrent.locks.ReentrantLock;

public class Doctor {
	private final ReentrantLock queue = new ReentrantLock();
	private int number;
	public Doctor(int number) {
		this.number = number;
	}

	public void take() {
		queue.lock();
	}

	public void leave() {
		queue.unlock();
	}

	public int getID() {
		return number;
	}

	public boolean isFree() {
		return !queue.isLocked();
	}
}
