import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.file.Files;

public class MainClass {

	public static void main(String[] args) throws IOException {
		/*
		 * primo argomento -> porta del server se non presente uso la 8080
		 */
		int port = 8080;
		if (args.length > 0)
			port = Integer.parseInt(args[0]);
		final ServerSocket server = new ServerSocket(port);
		boolean cond = true;
		System.out.println("Running on port " + port);
		while (cond) {
			final Socket client = server.accept();
			InputStreamReader isr = new InputStreamReader(
					client.getInputStream());
			try {
				BufferedReader reader = new BufferedReader(isr);
				/*
				 * estraggo il nome del file che richiedo dal path dell'url
				 * della richiest
				 * 
				 */
				String result = extractFileName(reader.readLine());
				DataOutputStream out = new DataOutputStream(
						client.getOutputStream());
				// se la richiesta è vuota rispondo con not found
				// altrimenti getImage cerca l'iimagine
				// e la invia tramite out
				if (result.length() == 0 || !getImage(result, out)) {
					out.writeBytes(
							"HTTP/1.1 404 \r\nConnection: close\r\n\r\n<h1>File not found</h1>");
				}
			} catch (SocketException e) {
				// nel caso la connessione venga annullata dal client
				System.err.println(e.getMessage());
			} finally {
				client.close();
				isr.close();
			}
		}
		server.close();
	}

	// estraggo il nome del file
	// che si trova fra / e uno spazio
	private static String extractFileName(String str) {
		int beginIndex = str.indexOf('/') + 1;
		int endIndex = str.lastIndexOf(" ");
		if (beginIndex == -1 || endIndex == -1
				|| endIndex < beginIndex)
			return null;
		return str.substring(beginIndex, endIndex);
	}

	private static boolean getImage(String name, DataOutputStream out)
			throws IOException {
		// img è la cartella che contiene i file da servire
		File dir = new File("img");
		for (File file : dir.listFiles()) {
			String fileName = file.getName();
			if (file.isFile() && name.equalsIgnoreCase(fileName)) {
				readFile(file, out);
				return true;
			}
		}
		// se l'immagine non è presente ritorno false
		return false;

	}

	private static void readFile(File file, DataOutputStream out) {
		try {
			// Leggo il file
			FileInputStream fileStream = new FileInputStream(file);
			// Dichiaro un array di byte che contenga tutto il file
			byte[] arr = new byte[(int) file.length()];
			// Metto il file nel buffer
			fileStream.read(arr);
			// Estraggo il content type
			String type = Files.probeContentType(file.toPath());
			// Scrivo gli header necessari
			out.writeBytes("HTTP/1.1 200 OK\r\n");
			out.writeBytes("Connection: keep-alive\r\n");
			if (type != null)
				out.writeBytes("Content-Type: " + type + "\r\n");
			out.writeBytes(
					"Content-Length: " + file.length() + "\r\n\r\n");
			out.write(arr);
			fileStream.close();
			out.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

}
