import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Main {
    public static final String Green = "\u001B[32m";
    public static final String Default = "\u001B[0m";
    public static final String Red = "\u001B[31m";
    public static final String Cyan = "\u001B[36m";
    public static final String Yellow = "\u001B[33m";
    public static final String Blue = "\u001B[34m";
    public static final String BackgroundBlack = "\u001B[40m";
    public static final String[] categories = { "prima", "seconda", "terza", "quarta", "quinta" };
    public static final String[] friends = { "diaz", "didaco", "didia", "didima", "didimo", "didio", "diega", "diego",
            "diletta", "diletto", "dilia", "mirko" };

    /**
    * REQUIRES: subData classe di tipo T, con T che estende Data
    * THROWS:   InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
    *           NoSuchMethodException, SecurityException
    * EFFECTS:  prende in input una classe T che estende data
    *           e ritorna una nuova istanza della classe passata
    *           funziona con le classi che estendono Data
    *           a patto che nello switch venga aggiunto come inizializzare la classe
    */
    public static <T extends Data> T getIstanceOfData(Class<T> subData)
            throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException {
        T result = null;
        switch (subData.getName()) {
        case "ImprovedData":
            List<String> tags = new ArrayList<>();
            tags.add("Programmazione");
            tags.add("Università");
            result = subData.getDeclaredConstructor(String.class, String.class, List.class).newInstance("Pacioni",
                    "Relazione progetto Java", tags);
            break;
        default:
            result = subData.getDeclaredConstructor().newInstance();
            break;
        }
        return result;
    }

    /**
     * REQUIRES: subBoard classe che estende DataBoard, subData classe di tipo T, con T che estende Data
     * EFFECTS: testa tutti i metodi descritti in DataBoard con le varie eccezioni
     *          prende in input qualsiasi classe implementi DataBoard e qualsiasi dato estenda Data
     */
    /**
    * Method tested:
    * createCategory
    * removeCategory
    * addFriend
    * put
    * insertLike
    * getDataCategory
    * getFriendIterator
    * getDataCategory
    * get
    * remove
    * removeFriend
    */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T extends Data> void test(Class<? extends DataBoard> subBoard, Class<T> subData) {
        DataBoard<T> implementation = null;
        String passw = "giusta";
        ArrayList<String> attempts = new ArrayList<>();
        attempts.add(null);
        attempts.add("");
        attempts.add(passw);

        // controllo con 3 password diverse (contenute in attempts) che le varie eccezioni vengano lanciate
        for (String password : attempts) {
            try {
                implementation = subBoard.getDeclaredConstructor(String.class).newInstance(password);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | NoSuchMethodException
                    | SecurityException fatal) {
                System.err.println("FATAL ERROR\t" + fatal.toString());
                System.exit(1);
            } catch (InvocationTargetException e) {
                System.out.println("Password: \"" + password + "\"");
                System.err.println(Red + e.getCause() + Default);
            }
        }
        // controllo sempre con attempts che le varie eccezioni vengano lanciate quando aggiungo una categoria
        for (String category : attempts) {
            try {
                implementation.createCategory(category, passw);
                implementation.createCategory(category, passw);
            } catch (Exception e) {
                System.out.println("Category: \"" + category + "\"");
                System.err.println(Red + e.getMessage() + Default);
            }
        }
        // controllo che removeCategory lanci un'eccezione quando cerco di rimuovere una categoria già cancellata
        String mare = "mare";
        try {
            implementation.createCategory(mare, passw);
            implementation.removeCategory(mare, passw);
            implementation.removeCategory(mare, passw);
        } catch (Exception e) {
            System.out.println(mare + " removed twice");
            System.err.println(Red + e.getMessage() + Default);
        }
        // controllo che addFriend lanci un'eccezione quando cerco di aggiungere un amico ad una categoria non presente
        try {
            implementation.createCategory(mare, passw);
            implementation.addFriend("scuola", passw, "riccardo");
        } catch (Exception e) {
            System.out.println("scuola was not created before");
            System.err.println(Red + e.getMessage() + Default);
        }
        T data = null;
        try {
            // aggiungo un po' di amici alla categoria mare
            data = getIstanceOfData(subData);
            implementation.put(passw, data, mare);
            implementation.addFriend(mare, passw, "riccardo");
            implementation.addFriend(mare, passw, "nicola");
            implementation.addFriend(mare, passw, "daniele");
            // inserisco un po' di like 
            implementation.insertLike("riccardo", data);
            implementation.insertLike("nicola", data);
            // controllo che il dato non venga aggiunto due volte
            System.out.println("should be false " + Green + implementation.put(passw, data, mare) + Default);
            // inserisco due like dallo stesso amico sullo stesso dato, ma come si vedrà dalla println
            // qui sotto non viene aggiunto in quanto già presente
            implementation.insertLike("riccardo", data);
            Iterator<T> it = implementation.getFriendIterator("riccardo");
            while (it.hasNext())
                System.out.println(it.next().display());
            // controllo che il metodo remove lanci un'eccezione se chiamato
            it.remove();
        } catch (Exception e) {
            System.out.println("Remove method is not implemented");
            System.err.println(Red + e.getMessage() + Default);
        }

        try {
            // Aggingo un po' di categorie
            for (String cat : categories) {
                T d = getIstanceOfData(subData);
                implementation.createCategory(cat, passw);
                // aggingo il dato d alla categoria cat
                implementation.put(passw, d, cat);
                // Aggiungo un po' di amici alla categoria cat
                for (String friend : friends) {
                    implementation.addFriend(cat, passw, friend);
                }
                int index = (int) (Math.random() * friends.length);
                // Inserisco un like a caso dagli amici aggiunti
                implementation.insertLike(friends[index], d);
            }
            // Controllo che il metodo getIterator ritorni i dati in ordine di like
            System.out.println("Get ordered data");
            Iterator<T> it = implementation.getIterator(passw);
            while (it.hasNext())
                System.out.println(it.next().display());
        } catch (Exception e) {
            System.err.println(Red + "Exception unexpected\n" + e.getMessage() + Default);
        }
        try {
            // Controllo che getFriendIterator mi ritorni tutti i dati condivisi con uno specifico amico
            System.out.println("\nGet iterator from mirko");
            implementation.addFriend(mare, passw, "mirko");
            implementation.insertLike("mirko", data);
            Iterator<T> it = implementation.getFriendIterator("mirko");
            while (it.hasNext())
                System.out.println(it.next().display());
        } catch (Exception e) {
            System.err.println(Red + "Exception unexpected\n" + e.getMessage() + Default);
        }
        try {
            // Get existing data
            System.out.println("\ndata:\t" + implementation.get(passw, data));

            // Get non existing data, should return null
            T nonExisting = getIstanceOfData(subData);
            System.out.println("nonExisting:\t" + implementation.get(passw, nonExisting));

            // Removing existing data
            System.out.println("data:\t" + implementation.remove(passw, data));
            // Removing non existing data, should return null
            System.out.println("nonExisting:\t" + implementation.remove(passw, nonExisting));

            implementation.removeFriend(mare, passw, "nicola");

            Iterator<T> it = implementation.getFriendIterator("nicola");
            System.out.println("\nData shared with nicola");
            // Iterator should be empty because nicola was removed as friend
            while (it.hasNext())
                System.out.println(it.next().display());
        } catch (Exception e) {
            System.err.println(Red + "Exception unexpected\n" + e.getMessage() + Default);
        }
        System.out.println("END");
    }

    public static void main(String[] args) {
        try {
            System.out.println(BackgroundBlack + Cyan + "TESTING HASHBOARD" + Default);
            test(HashBoard.class, Data.class);

            System.out.println(Yellow + "\nTESTING ARRAYBOARD" + Default);
            test(ArrayBoard.class, Data.class);

            System.out.println(Blue + "\nTESTING ARRAYBOARD WITH IMPROVEDDATA" + Default);
            test(ArrayBoard.class, ImprovedData.class);
        } catch (Exception e) {
            System.err.println(Red + e.getMessage() + Default);
        }
    }

}