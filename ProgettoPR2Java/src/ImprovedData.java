import java.util.ArrayList;
import java.util.List;

class ImprovedData extends Data {
    /* OVERVIEW: Tipo mutabile che contiene una lista di stringhe che rappresentano i tag,
    *               una stringa che rappresenta l'autore ed un'altra che rappresenta il titolo
    * 
    * Typical Element:  < author, title, tags > dove
    * 					tags è una lista di stringhe che rappresenta gli amici che hanno messo like
    *                   author è una stringa contente l'autore
    *                   title è una stringa contente lil titolo
    * Rep Invariant:  RI(c) =   c.tags != null
    */
    String author;
    String title;
    List<String> tags;

    /**
    * EFFECTS:  inzializza il costruttore del super tipo, l'autore, il titolo e i tags con i parametri passati
    */
    public ImprovedData(String author, String title, List<String> tags) {
        super();
        this.author = author;
        this.title = title;
        this.tags = new ArrayList<>(tags);
    }

    /**
    * EFFECTS:  inzializza il costruttore del super tipo passandogli un vecchio Data da clonare,
    *           l'autore, il titolo e i tags con i parametri passati
    */
    public ImprovedData(String author, String title, List<String> tags, Data old) {
        super(old);
        this.author = author;
        this.title = title;
        this.tags = new ArrayList<>(tags);
    }

    /**
    * EFFECTS:  ritorna un nuovo improved data con gli stessi valori dell'oggetto stesso
    */
    @Override
    public ImprovedData clone() {
        return new ImprovedData(author, title, tags, (Data) this);
    }

    /**
    * EFFECTS:  ritorna una stringa contente titolo e autore
    */
    @Override
    public String toString() {
        return author + " - " + title;
    }

    /**
    * EFFECTS:  ritorna una stringa contente titolo, autore, tag e i dati del supertipo
    */
    @Override
    public String display() {
        return toString() + " " + tags + "\t" + super.display();
    }

}