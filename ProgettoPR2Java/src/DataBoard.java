import java.util.Iterator;
import java.util.List;

import javax.naming.AuthenticationException;

/**
 * OVERVIEW: Tipo modificabile che rappresenta la bacheca di un utente che
 * immagazzina gli oggetti di tipo E che estende Data
 * TIPICAL ELEMENT: {
 * < password,
 *  {
 *      <category_0, (data_0_0, ...), (friend_0_0, ...)>, ..., <category_n-1,(data_n-1_0, ...), (friend_n-1_0, ...)>
 *  }
 *  >
 * }
 * dove n è il numero di categorie nella bacheca
 * dove password è una stringa diversa da null e da ""
 * dove category_i != category_j per ogni i != j tale che 0 <= i <j < n 
 * dove (data_i_j, ... ) è una collezione di dati (che può essere anche vuota): (dato_i_0, dato_i_1, ...)
 * dove data_i_j != data_i_k per ogni i, per ogni j != k tale che  0 <= j < k
 * dove friend_i_j != friend_i_k per ogni i, per ogni j != k tale che  0 <= j < k
 */
public interface DataBoard<E extends Data> {

        // Crea l’identità una categoria di dati
        /**
         * REQUIRES: category != null, passw != null, category != "", passw != "", 
         * category non presente nella bacheca, password != passw
         * THROWS: NullPointerException se passw == null || category == null, 
         * IllegalArgumentException se category == "" || passw == "" || category è presente nella bacheca,
         * AuthenticationException se password == passw
         * MODIFIES: this 
         * EFFECTS: post(this) = pre(this) + <{category, (), ()}>
         */
        public void createCategory(String category, String passw)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        // Rimuove l’identità una categoria di dati
        /**
        * REQUIRES: category != null, passw != null, category != "", passw != "", 
        * category presente nella bacheca, password == passw
        * THROWS: NullPointerException se passw == null || category == null, 
        * IllegalArgumentException se category == "" || passw == "" || category non è presente nella bacheca,
        * AuthenticationException se password != passw
        * MODIFIES: this
        * EFFECTS: post(this) = pre(this) - < category, (...), (...)>
        */
        public void removeCategory(String category, String passw)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * REQUIRES: category != null, passw != null, friend != null, category != "", passw != "", friend != ""
        * category presente nella bacheca, password == passw, friend non presente nella lista degli amici
        * THROWS: NullPointerException se passw == null || category == null || friend == null, 
        * IllegalArgumentException se category == "" || passw == "" || friend == "" 
        * || category non è presente nella bacheca || friend è presente nella lista,
        * AuthenticationException se password != passw
        * MODIFIES: this
        * EFFECTS: post(this) = < password, { ..., < category, (...), (...) + friend >, ... }>
        */
        // Aggiunge un amico ad una categoria di dati
        public void addFriend(String category, String passw, String friend)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * REQUIRES: category != null, passw != null, friend != null, category != "", passw != "", friend != ""
        * category presente nella bacheca, password == passw, friend presente nella lista degli amici
        * THROWS: NullPointerException se passw == null || category == null || friend == null, 
        * IllegalArgumentException se category == "" || passw == "" || friend == "" 
        * || category non è presente nella bacheca || friend non è presente nella lista,
        * AuthenticationException se password != passw
        * MODIFIES: this
        * EFFECTS: post(this) = < password, { ..., < category, (...), (...) - friend > , ... }>
        */
        // Rimuove un amico ad una categoria di dati
        public void removeFriend(String category, String passw, String friend)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * REQUIRES: category != null, passw != null, data != null, category != "", passw != "",
        * category presente nella bacheca, password == passw.
        * THROWS: NullPointerException se category == null, passw == null, data == null
        * IllegalArgumentException se passw == "" || category == "" || category non è presente nella bacheca
        * AuthenticationException se password != passw.
        * MODIFIES: this
        * EFFECTS: post(this) = < password, { ..., < category, (...) + data, (...)> , ... }>
        * RETURNS: true se data è stato inserito, false se data era già presente e non è stato inserito
        */
        // Inserisce un dato in bacheca
        // se vengono rispettati i controlli di identità
        public boolean put(String passw, E data, String category)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * Ottiene una copia del dato in bacheca
        * se vengono rispettati i controlli di identità
        * REQUIRES: passw != null, data != null, pass != "", password == passw.
        * THROWS: NullPointerException se passw == null || data == null
        * AuthenticationException se password != passw.
        * RETURNS: una copia del dato stesso se presente, null se non presente
        */
        public E get(String passw, E data) throws NullPointerException, AuthenticationException;

        /**
        * Rimuove il dato dalla bacheca
        * se vengono rispettati i controlli di identità
        * REQUIRES: passw != null, data != null, passw != "", password == passw.
        * THROWS: NullPointerException se passw == null, data == null
        * IllegalArgumentException se passw == ""
        * AuthenticationException se password != passw.
        * MODIFIES: this
        * EFFECTS: post(this) = < password, { ..., < category_i, (...) - data, (...)> , ... }>
        * per ogni category_i che ha data nella sua lista
        * RETURNS: il dato stesso se presente, null se non presente
        */
        public E remove(String passw, E data)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * Crea la lista dei dati in bacheca su una determinata categoria
        * se vengono rispettati i controlli di identità
        * REQUIRES: passw != null, passw != "", password == passw, category != null
        * THROWS: NullPointerException se passw == null, category == null
        * IllegalArgumentException se passw == ""
        * AuthenticationException se password != passw.
        * RETURNS: la lista di dati associata a category
        */
        public List<E> getDataCategory(String passw, String category)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * restituisce un iteratore (senza remove) che genera tutti i dati in
        * bacheca ordinati rispetto al numero di like.
        * REQUIRES: passw != null, passw != "", password == passw
        * THROWS: NullPointerException se passw == null
        * IllegalArgumentException se passw == ""
        * AuthenticationException se password != passw.
        * RETURNS: un iteratore senza remove che contiene tutti i dati in ordine di like
        */
        public Iterator<E> getIterator(String passw)
                        throws NullPointerException, IllegalArgumentException, AuthenticationException;

        /**
        * Aggiunge un like a un dato
        * REQUIRES: friend != null, friend != "", data != null
        * THROWS: NullPointerException se data == null || friend == null
        * IllegalArgumentException se friend == ""
        * MODIFIES: this
        * EFFECTS: Aggiunge un like da friend a un dato E
        */
        public void insertLike(String friend, E data) throws NullPointerException, IllegalArgumentException;

        /**
        * Legge un dato condiviso restituisce un iteratore (senza remove) che genera tutti i dati in
        * bacheca condivisi.
        * REQUIRES: friend != null, friend != ""
        * THROWS: NullPointerException se  friend == null
        * IllegalArgumentException se friend == ""
        * RETURNS: un iteratore senza remove contenente tutti i dati condivisi con friend
        * ritorna un iteratore vuoto se friend non compare
        */
        public Iterator<E> getFriendIterator(String friend) throws NullPointerException, IllegalArgumentException;

        // ... altre operazione da definire a scelta
}