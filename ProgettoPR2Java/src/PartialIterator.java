import java.util.Iterator;

class PartialIterator<E> implements Iterator<E> {
    /* OVERVIEW: Tipo immutabile che contiene una iteratore di dati 
    * 
    * Typical Element:  < it > dove
    * 					it è un iteratore
    * Rep Invariant:  RI(c) =    c.it != null
    */
    Iterator<E> it = null;

    /**
    * REQUIRES: iterator != null
    * THROWS: NullPointerException se l'iteratore passato è null
    * MODIFIES: this.it
    */
    public PartialIterator(Iterator<E> iterator) {
        if (iterator == null)
            throw new NullPointerException("Iterator is null");
        it = iterator;
    }

    /**
    * EFFECTS: true se l'elemento corrente ha un elemento successivo
    */
    @Override
    public boolean hasNext() {
        return it.hasNext();
    }

    /**
    * THROWS: NoSuchElementException se it non ha più elementi 
    * EFFECTS: ritorna l'elemento successivo
    */
    @Override
    public E next() {
        return it.next();
    }

    /**
    * THROWS: UnsupportedOperationException in quanto l'operazione di remove non è supportata
    */
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is not implemented");
    }
}