import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.naming.AuthenticationException;

@SuppressWarnings("unchecked")
class HashBoard<E extends Data> implements DataBoard<E> {
    /* OVERVIEW: Tipo modificabile che rappresenta la bacheca di un utente che
     * immagazzina gli oggetti di tipo E che estende Data
     * 
     * AF(c) = f: String -> Container<E> tale che f(i) = c.map.get(i) 
     *          per i in c.map.keySet() e indefinito altrimenti
     * 
     * IR(c) =  c.map != null
     *          && c.password != null
     *          && c.password != "" 
     * 			&& (for all i, j in c.categories ==> i != null && j != null && i != j)
     * 			&& (for all i. i in c.categories.keySet() ==> c.categories.get(i) != null)
     */
    private HashMap<String, Container<E>> map;
    private String password;

    /**
    * REQUIRES: password != null && password !=""
    * THROWS: NullPointerException se password == null || InvalidParameterException se password == ""
    * MODIFIES: this
    * EFFECTS: inizializza this.map a HashMap vuota e this.password = password
    */
    public HashBoard(String password) throws NullPointerException, InvalidParameterException {
        if (password == null)
            throw new NullPointerException("Password is null");
        if (password.equals(""))
            throw new InvalidParameterException("Password is empty");
        this.password = password;
        this.map = new HashMap<String, Container<E>>();
    }

    public void createCategory(String category, String passw)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (map.containsKey(category))
            throw new IllegalArgumentException("Category already inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        map.put(category, new Container<E>());
    }

    public void removeCategory(String category, String passw)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!map.containsKey(category))
            throw new IllegalArgumentException("Category not inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        map.remove(category);
    }

    public void addFriend(String category, String passw, String friend)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        if (!map.containsKey(category))
            throw new IllegalArgumentException("Category not inserted");
        if (map.get(category).containsFriend(friend))
            throw new IllegalArgumentException("Friend alreay added");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        map.get(category).addFriend(friend);
    }

    public void removeFriend(String category, String passw, String friend)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        if (!map.containsKey(category))
            throw new IllegalArgumentException("Category not inserted");
        if (!map.get(category).containsFriend(friend))
            throw new IllegalArgumentException("Friend not found");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        map.get(category).removeFriend(friend);
    }

    public boolean put(String passw, E data, String category)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!map.containsKey(category))
            throw new IllegalArgumentException("Category not inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        return map.get(category).addData((E) data.clone());
    }

    public E get(String passw, E data) throws NullPointerException, AuthenticationException {
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        for (Container<E> container : map.values()) {
            if (container.containsData(data))
                return (E) data.clone();
        }
        return null;
    }

    public E remove(String passw, E data)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        E found = null;
        for (Container<E> container : map.values()) {
            if (container.removeData(data)) {
                found = data;
            }
        }
        return found;
    }

    public List<E> getDataCategory(String passw, String category)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!map.containsKey(category))
            throw new IllegalArgumentException("Category not inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        return map.get(category).getData();
    }

    public Iterator<E> getIterator(String passw)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        TreeSet<E> cont = new TreeSet<>();
        for (Container<E> container : map.values()) {
            cont.addAll(container.getData());
        }
        List<E> result = new ArrayList<>(cont);
        result.sort(Comparator.comparingInt(Data::getLikes).reversed());
        return new PartialIterator<>(result.iterator());
    }

    public void insertLike(String friend, E data) throws NullPointerException, IllegalArgumentException {
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        for (Container<E> container : map.values()) {
            if (container.containsFriend(friend) && container.containsData(data)) {
                container.addLike(data, friend);
            }
        }
    }

    public Iterator<E> getFriendIterator(String friend) throws NullPointerException, IllegalArgumentException {
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        TreeSet<E> result = new TreeSet<>();
        for (Container<E> container : map.values()) {
            if (container.containsFriend(friend)) {
                result.addAll(container.getData());
            }
        }
        return new PartialIterator<>(Collections.unmodifiableSortedSet(result).iterator());
    }
}