import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.naming.AuthenticationException;

@SuppressWarnings("unchecked")
class ArrayBoard<E extends Data> implements DataBoard<E> {
    /* OVERVIEW: Tipo modificabile che rappresenta la bacheca di un utente che
     * immagazzina gli oggetti di tipo E che estende Data
     * 
     * AF(c) = f: String -> Container<E> tale che f(c.categoris.get(i)) = c.containers.get(i) 
     *          per i in c.categories e indefinito altrimenti
     * 
     * IR(c) =  c.categories != null
     *          && c.containers != null
     *          && c.password != null
     *          && c.password != "" 
     * 			&& (for all i, j in c.categories ==> i != null && j != null && i != j)
     * 			&& (for all i. i in c.containers ==> c.containers.get(i) != null)
     */

    private ArrayList<String> categories;
    private ArrayList<Container<E>> containers; // Lista di container che contengono a sua volta la lista di amici e di dati
    private String password;

    /**
    * REQUIRES: password != null && password !=""
    * THROWS: NullPointerException se password == null || InvalidParameterException se password == ""
    * MODIFIES: this
    * EFFECTS: inizializza this.categories e this.containers a ArrayList vuoti e this.password = password
    */
    public ArrayBoard(String password) throws NullPointerException, InvalidParameterException {
        if (password == null)
            throw new NullPointerException("Password is null");
        if (password.equals(""))
            throw new InvalidParameterException("Password is empty");
        this.password = password;
        this.categories = new ArrayList<>();
        this.containers = new ArrayList<>();
    }

    public void createCategory(String category, String passw)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (categories.contains(category))
            throw new IllegalArgumentException("Category already inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        categories.add(category);
        containers.add(new Container<>());
    }

    public void removeCategory(String category, String passw)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!categories.contains(category))
            throw new IllegalArgumentException("Category not inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        int index = categories.indexOf(category);
        categories.remove(index);
        containers.remove(index);
    }

    public void addFriend(String category, String passw, String friend)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        int index = categories.indexOf(category);
        if (index == -1)
            throw new IllegalArgumentException("Category not inserted");
        if (containers.get(index).containsFriend(friend))
            throw new IllegalArgumentException("Friend alreay added");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        containers.get(index).addFriend(friend);
    }

    public void removeFriend(String category, String passw, String friend)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        int index = categories.indexOf(category);
        if (index == -1)
            throw new IllegalArgumentException("Category not inserted");
        if (!containers.get(index).containsFriend(friend))
            throw new IllegalArgumentException("Friend not found");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        containers.get(index).removeFriend(friend);
    }

    public boolean put(String passw, E data, String category)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        int index = categories.indexOf(category);
        if (index == -1)
            throw new IllegalArgumentException("Category not inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        return containers.get(index).addData((E) data.clone());
    }

    public E get(String passw, E data) throws NullPointerException, AuthenticationException {
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        E result = null;
        for (Container<E> container : containers) {
            if (container.containsData(data))
                return (E) data.clone();
        }
        return result;
    }

    public E remove(String passw, E data)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        E result = null;
        for (Container<E> container : containers) {
            if (container.removeData(data)) {
                result = data;
            }
        }
        return result;
    }

    public List<E> getDataCategory(String passw, String category)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (category == null)
            throw new NullPointerException("Category is null");
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (category.equals(""))
            throw new IllegalArgumentException("Category is empty");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        int index = categories.indexOf(category);
        if (index == -1)
            throw new IllegalArgumentException("Category not inserted");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        return containers.get(index).getData();
    }

    public Iterator<E> getIterator(String passw)
            throws NullPointerException, IllegalArgumentException, AuthenticationException {
        if (passw == null)
            throw new NullPointerException("Password is null");
        if (passw.equals(""))
            throw new IllegalArgumentException("Password is empty");
        if (!password.equals(passw))
            throw new AuthenticationException("Wrong password");
        TreeSet<E> cont = new TreeSet<>();
        for (Container<E> container : containers) {
            cont.addAll(container.getData());
        }
        List<E> result = new ArrayList<>(cont);
        result.sort(Comparator.comparingInt(Data::getLikes).reversed());
        return new PartialIterator<>(result.iterator());
    }

    public void insertLike(String friend, E data) throws NullPointerException, IllegalArgumentException {
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (data == null)
            throw new NullPointerException("Data is null");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        for (Container<E> container : containers) {
            if (container.containsFriend(friend) && container.containsData(data)) {
                container.addLike(data, friend);
            }
        }
    }

    public Iterator<E> getFriendIterator(String friend) throws NullPointerException, IllegalArgumentException {
        if (friend == null)
            throw new NullPointerException("Friend is null");
        if (friend.equals(""))
            throw new IllegalArgumentException("Friend is empty");
        SortedSet<E> result = new TreeSet<E>();
        for (Container<E> container : containers) {
            if (container.containsFriend(friend)) {
                for (E e : container.getData()) {
                    result.add((E) e.clone());
                }
            }
        }
        return new PartialIterator<>(Collections.unmodifiableSortedSet(result).iterator());
    }
}