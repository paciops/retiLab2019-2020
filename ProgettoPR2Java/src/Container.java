import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

class Container<V extends Data> {
    /* OVERVIEW: Tipo mutabile che contiene una lista di dati E ed una lista di amici
    *           (modellati come stringhe) che possono interagire con i dati
    * 
    * Typical Element:  < friends , elements > dove
    * 					friends è un set di stringhe che contiene gli amici che possono vedere i dati
    * 					elements è un set di V (che estende Data) che contiene i dati
    * 
    * Rep Invariant:  
    * 		RI(c) =    c.passw != null 
    * 				&& c.elems != null
    * 				&& (for all i. 0 <= i < c.elements.size() ==> elements.get(i) != null)
    * 				&& (for all i. 0 <= i < c.friends.size() ==> friends.get(i) != null)
    */
    private TreeSet<String> friends;
    private TreeSet<V> elements;

    /**
    * MODIFIES: this
    * EFFECTS:  friends viene inizializzato ad un set di stringhe vuoto
        elements viene inizializzato ad un set di elementi V vuoto
    */
    public Container() {
        friends = new TreeSet<String>();
        elements = new TreeSet<V>();
    }

    /**
    * EFFECTS: ritorna true se friend è contenuto in friends
    */
    public boolean containsFriend(String friend) {
        return friends.contains(friend);
    }

    /**
    * EFFECTS: ritorna true se data è contenuto in elements
    */
    public boolean containsData(V data) {
        return elements.contains(data);
    }

    /**
    * MODIFIES: this.friends
    * EFFECTS:  ritorna true se friend viene aggiunto a friends, false se friend era già presente
    */
    public boolean addFriend(String friend) {
        return friends.add(friend);
    }

    /**
    * MODIFIES: this.elements
    * EFFECTS:  ritorna true se data viene aggiunto a elements, false se data era già presente
    */
    public boolean addData(V data) {
        return elements.add(data);
    }

    /**
    * MODIFIES: this.friends
    * EFFECTS: ritorna true se friend viene rimosso da friends, false se friend non era presente in friends
    */
    public boolean removeFriend(String friend) {
        return friends.remove(friend);
    }

    /**
    * MODIFIES: this.elements
    * EFFECTS: ritorna true se data viene rimosso da elements, false se data non era presente in elements
    */
    public boolean removeData(V data) {
        return elements.remove(data);
    }

    /**
    * MODIFIES: this.elements
    * EFFECTS: aggiunge al dato elem, se presente, un amico alla lista di like, in caso elem non fosse presente non fa nulla
    */
    public void addLike(V elem, String friend) {
        V tmp = elements.floor(elem);
        if (tmp != null)
            tmp.addLike(friend);
    }

    /**
    * EFFECTS: ritorna tutti i dati contenuti in elements clonati in una lista non modificabile
    */
    public List<V> getData() {
        return Collections.unmodifiableList(new ArrayList<>(elements));
    }
}