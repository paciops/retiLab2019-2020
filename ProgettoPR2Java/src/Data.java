import java.util.ArrayList;
import java.util.UUID;

public class Data implements Comparable<Data> {
    /* OVERVIEW: Tipo mutabile che contiene una lista di amici che 
    *               hanno messo like ed una stringa che contiene l'id
    * Typical Element:  < likes, id > dove
    * 					likes è una lista di stringhe che rappresenta gli amici che hanno messo like
    *                   id è una stringa contenente un UUID https://docs.oracle.com/javase/7/docs/api/java/util/UUID.html#randomUUID()
    * Rep Invariant:  RI(c) =   c.id != null
    *                           && c.likes != null
    */
    private ArrayList<String> likes;
    protected String id;

    /**
    * MODIFIES: this
    * EFFECTS: inizializza likes ad una lista di stringhe vuota e id ad un UUID casuale
    */
    public Data() {
        this.likes = new ArrayList<String>();
        this.id = UUID.randomUUID().toString();
    }

    /**
    * MODIFIES: this
    * EFFECTS: crea un nuovo oggetto data con stesso id e stesso array di like
    */
    public Data(Data old) {
        likes = new ArrayList<String>();
        id = old.id;
        for (String friend : old.likes)
            likes.add(friend);
    }

    /**
    * MODIFIES: this
    * EFFECTS:  se friend non ha già messo like, viene aggiunto alla lista di like
    */
    public void addLike(String friend) {
        if (!likes.contains(friend))
            likes.add(friend);
    }

    /**
    * MODIFIES: this
    * EFFECTS:  ritorna la lunghezza della lista di dati
    */
    public int getLikes() {
        return likes.size();
    }

    /**
    * EFFECTS:  ritorna una stringa contentente le informazioni di questo dato
                ad esempio l'id, la quantità di like e chi ha messo like
    */
    public String display() {
        int n = getLikes();
        return id + "\t" + n + " " + (n > 1 ? "likes" : "like") + " from "
                + likes.toString().replace("[", "").replace("]", "");
    }

    /**
    * EFFECTS:  clona l'oggetto utilizzando il construttore che prende un tipo Data come parametro
    */
    @Override
    public Data clone() {
        return new Data(this);
    }

    /**
    * EFFECTS:  compara l'id di se stesso con l'id dell'oggetto passato
    */
    @Override
    public int compareTo(Data compare) {
        return id.compareTo(compare.id);
    }

    /**
    * EFFECTS:  ritorna l'id dell'oggetto
    */
    @Override
    public String toString() {
        return id;
    }
}
