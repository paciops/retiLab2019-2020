import java.io.File;
import java.io.IOException;

public class MainClass {

	public static void main(String[] args) {
		// il programma esce se non viene passato il path
		if (args.length < 1) {
			System.err.println("Missing path as first argument");
			System.exit(-1);
		}
		String path = args[0];
		File dir = new File(path);
		// esce se il path passato non è una directory
		if (!dir.isDirectory()) {
			System.err.println(path + " is not a directory");
			System.exit(-2);
		}
		try {
			FolderQueue queue = new FolderQueue();
			// prende il path assoluto
			path = new File(path).getCanonicalPath();
			Crawler crawler = new Crawler(queue, path);
			Thread threadCrawler = new Thread(crawler);
			// numero casuale di thread
			int k = (int) (Math.random() * 9) + 1;
			System.out.println("K:" + k);
			Thread[] printers = new Thread[k];
			for (Thread printer : printers) {
				printer = new Thread(new Printer(queue));
				printer.start();
			}
			threadCrawler.start();
		} catch (IOException e) {
			// in caso il file non fosse presente lancio l'eccezione
			e.printStackTrace();
			System.exit(-3);
		}
	}

}
