import java.io.File;

public class Printer extends Thread {
	private FolderQueue queue;
	public Printer(FolderQueue queue) {
		this.queue = queue;
	}

	public void run() {
		String name = Thread.currentThread().getName();
		while (queue.isNotTeminated()) {
			String path = queue.get();
			if (path != null) {
				File dir = new File(path);
				File[] files = dir.listFiles();
				for (File file : files) {
					if (file.isFile()) {
						System.out.println(name + "\tfile:\t" + file);
					}
				}
			}
		}
	}
}
