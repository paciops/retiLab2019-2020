import java.io.File;
import java.util.ArrayList;

public class Crawler extends Thread {
	public Crawler(FolderQueue queue, String path) {
		super();
		this.queue = queue;
		this.path = path;
	}
	private String path;
	private FolderQueue queue;

	public void run() {
		// utilizzo un arraystring all'interno del thread che
		// scansiona le cartelle
		ArrayList<String> dirs = new ArrayList<String>();
		// aggiungo la cartella corrente alla lista
		// così facendo anche i file in questa cartella vengono
		// stampati
		dirs.add(path);
		// ciclo finchè non ho più cartelle
		while (!dirs.isEmpty()) {
			int index = 0;
			// estraggo sempre la cartella in testa
			String lastElem = dirs.get(index);
			// cerco le sottocartelle della cartella
			dirs.addAll(getDirs(lastElem));
			if (!dirs.remove(lastElem)) {
				// do un errore se non riesco a rimuovere una
				// cartella dalla lista
				System.err.println(lastElem + " ERROR");
			} else {
				queue.add(lastElem);
			}
		}
		queue.end();
		System.out.println("crawler termina");
	}

	private ArrayList<String> getDirs(String path) {
		// passo una cartella e ritorno tutte le sottocartelle 
		ArrayList<String> dirs = new ArrayList<String>();
		File dir = new File(path);
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (File file : files)
				if (file.isDirectory())
					dirs.add(file.getPath());
		}
		return dirs;
	}
}
