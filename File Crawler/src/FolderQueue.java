import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class FolderQueue {
	private LinkedList<String> list;
	private ReentrantLock elementLock;
	private ReentrantLock statusLock;
	private Condition elementCondition;
	private Condition statusCondition;
	private boolean ended = false;
	public FolderQueue() {
		list = new LinkedList<String>();
		elementLock = new ReentrantLock();
		statusLock = new ReentrantLock();
		elementCondition = elementLock.newCondition();
		statusCondition = statusLock.newCondition();
	}

	public void add(String path) {
		elementLock.lock();
		list.addLast(path);
		elementCondition.signalAll();
		elementLock.unlock();
	}

	public String get() {
		elementLock.lock();
		try {
			// aspetto finche la lista non è vuota 
			// e il crawler non ha finito di inserire
			// le cartelle 
			while (list.isEmpty() && !ended)
				elementCondition.await();
			// in caso la lista sia vuota poll
			// ritorna null
			return list.poll();
		} catch (InterruptedException e) {
			e.printStackTrace();
			// viene ritornato null anche se
			// il thread viene interrotto
			return null;
		} finally {
			elementLock.unlock();
		}
	}

	public void end() {
		statusLock.lock();
		ended = true;
		statusCondition.signalAll();
		statusLock.unlock();
	}

	public boolean isNotTeminated() {
		statusLock.lock();
		// per i thread il programma non è terminato
		// solo se il crawler non ha finito o la lista
		// non è vuota
		boolean res = !ended || !list.isEmpty();
		statusLock.unlock();
		return res;
	}
}
