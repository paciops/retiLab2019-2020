package app;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.util.Vector;
import java.util.List;

class Session implements Serializable {
    private static final long serialVersionUID = -5852996951352660752L;
    private List<String> speakers;
    private final int LIMIT = 5;

    public Session() {
        speakers = new Vector<>(LIMIT);
    }

    public boolean addSpeaker(String speaker) {
        if (speaker == null)
            throw new NullPointerException("Speaker is null");
        if (speakers.size() == LIMIT || speakers.contains(speaker))
            return false;
        return speakers.add(speaker);
    }

    public List<String> getSpeakes() {
        return speakers;
    }

    public String toString() {
        return "There are " + speakers.size() + " people that are going to speak\t" + speakers;
    }
}

class Day {
    private final int LIMIT = 12;
    private List<Session> sessions;

    public Day() {
        sessions = new Vector<>(LIMIT);
    }

    public boolean addSession() {
        if (sessions.size() == LIMIT)
            return false;
        return sessions.add(new Session());
    }

    public Session getSession(int i) {
        return sessions.get(i);
    }

    public boolean addSpeaker(int session, String speaker) {
        return sessions.get(session).addSpeaker(speaker);
    }

}

interface CongressInterface extends Remote {

    public boolean addDay() throws RemoteException;

    public boolean addSession(int day) throws RemoteException;

    public Session getSession(int day, int session) throws RemoteException;

    public String getProgramm() throws RemoteException;

    public boolean addSpeaker(int day, int session, String speaker) throws RemoteException;

}

class Congress extends RemoteServer implements CongressInterface {
    private static final long serialVersionUID = 163566009293901405L;
    private List<Day> days;

    public Congress() {
        days = new Vector<>(3);
    }

    @Override
    public boolean addSession(int day) throws RemoteException {
        return days.get(day).addSession();
    }

    @Override
    public Session getSession(int day, int session) throws RemoteException {
        return days.get(day).getSession(session);
    }

    @Override
    public boolean addSpeaker(int day, int session, String speaker) throws RemoteException {
        return days.get(day).addSpeaker(session, speaker);
    }

    @Override
    public boolean addDay() throws RemoteException {
        if (days.size() == 3)
            return false;
        return days.add(new Day());
    }

    @Override
    public String getProgramm() throws RemoteException {
        String result = new String();
        int num = 0;
        for (Day day : days) {
            num++;
            result += "Day " + num + "\n";
            for (int i = 0; i < 12; i++) {
                result += i + 1 + ")";
                for (String speaker : day.getSession(i).getSpeakes()) {
                    result += (speaker + "\t");
                }
                result += "\n";
            }
            result += "\n";
        }
        return result;
    }

}