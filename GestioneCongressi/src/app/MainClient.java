package app;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.NoSuchElementException;
import java.util.Scanner;

class MainClient {
    /**
     * throw away the \n not consumed by nextInt()
     * @param console scanner of system.in
     */
    public static void throwAwayInput(Scanner console) {
        console.nextLine();
    }

    /**
     * 
     * @param args first element must be the port
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("First argument must be the host");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        // security manager, che è?
        try {
            Registry registry = LocateRegistry.getRegistry(port);
            CongressInterface congressInterface = (CongressInterface) registry.lookup("CONGRESS-SERVER");
            // System.out.println("Tento di aggiungere un giorno " + congressInterface.addDay());
            boolean cond = true;
            Scanner console = new Scanner(System.in);
            int day = -1, session = -1;
            while (cond) {
                try {
                    System.out.println("\nadd speaker: add speaker at given day and given session");
                    System.out.println("get programm: get programm at given day and given session");
                    System.out.println("get general programm: get programm of alll three days");
                    System.out.println("CTRL+D to exit\n");
                    String operation = console.nextLine();
                    switch (operation) {
                    case "add speaker":
                        System.out.println("Which day? [1-3]");
                        day = console.nextInt();
                        System.out.println("Which sessions?");
                        session = console.nextInt();
                        System.out.println("Name of the speaker? [1-12]");
                        throwAwayInput(console);
                        String speaker = console.nextLine();
                        if (congressInterface.addSpeaker(day - 1, session - 1, speaker))
                            System.out.println(speaker + " added successfully");
                        else
                            System.out.println(speaker + " is not added, session is full");
                        break;
                    case "get programm":
                        System.out.println("Which day? [1-3]");
                        day = console.nextInt();
                        System.out.println("Which sessions? [1-12]");
                        session = console.nextInt();
                        throwAwayInput(console);
                        System.out.println(congressInterface.getSession(day - 1, session - 1));
                        break;
                    case "get general programm":
                        System.out.println(congressInterface.getProgramm());
                        break;
                    default:
                        System.err.println("\n\tOperation not valid\n");
                        break;
                    }
                } catch (NoSuchElementException e) {
                    System.out.println("Exit");
                    cond = false;
                } catch (IndexOutOfBoundsException e) {
                    System.err.println("\n\tDay or session is invalid\n");
                }
            }
            console.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}