package piGreco;

public class Calculator implements Runnable {
	private double accuracy;
	public Calculator(double accuracy) {
		this.accuracy = accuracy;
	}
	
	@Override
	public void run() {
		double pi = 4.0;
		boolean sign = false;
		double num = 3;
		while(Math.abs(Math.PI - pi) >= accuracy && !Thread.interrupted()) {
			pi = pi + (sign? (4/num): (-4/num));
			num +=2;
			sign = !sign;			
		}
		System.out.println(pi);
	}
	
	public static void main(String[] args) throws InterruptedException {
		if(args.length != 2) throw new Error("Missing accuracy and time to live");
		int exp = Integer.parseInt(args[0]);
		int ttl = Integer.parseInt(args[1]);
		double accuracy = Math.pow(10, -exp);
		Calculator calc = new Calculator(accuracy);
		Thread thread = new Thread(calc);
		thread.start();
		Thread.sleep(ttl);
		thread.interrupt();
	}
}
