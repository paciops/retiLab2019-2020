package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import database.JSONDatabase;
import exception.AlreadyFriendsException;
import exception.UserNotFoundException;
import logger.Logger;
import parameters.Parameters;

public class RequestHandler {
	private static final int MAX_CAPACITY = 16;
	private SocketChannel client;
	private SelectionKey key;
	private Selector selector;
	private ServerSocketChannel serverSocket;
	private JSONDatabase db;
	private Parameters params;
	private Logger logger;

	private Map<String, Number> ports;
	private Map<Integer, String> online;
	private Map<Integer, Future<Boolean>> threadMap;
	private List<Game> games;
	private ExecutorService threadUDPExecutor;

	public RequestHandler(Parameters params) throws IOException {
		this.params = params;
		ports = new ConcurrentHashMap<>();
		online = new ConcurrentHashMap<>();
		threadMap = new ConcurrentHashMap<>();
		games = new ArrayList<>();
		threadUDPExecutor = Executors.newCachedThreadPool();
		db = new JSONDatabase();
		logger = new Logger("[37;44m");
		selector = Selector.open();
		serverSocket = ServerSocketChannel.open();
		serverSocket.bind(new InetSocketAddress("localhost", 5454));
		serverSocket.configureBlocking(false);
		serverSocket.register(selector, SelectionKey.OP_ACCEPT);
	}

	private void read() throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(MAX_CAPACITY);
		client = (SocketChannel) key.channel();
		Object pastAttachment = key.attachment();
		String request = (pastAttachment == null)
				? ""
				: (String) pastAttachment;
		int bytesRead = client.read(buffer);
		buffer.flip();
		request += StandardCharsets.UTF_8.decode(buffer).toString();
		buffer.clear();
		if (bytesRead == MAX_CAPACITY) {
			// the request is not complete
			key.attach(request);
		} else if (bytesRead == -1 && request.equals("")) {
			// connection interrupted
			String nickname = online.remove(key.hashCode());
			if (nickname != null)
				ports.remove(nickname);
			client.close();
		} else {
			logger.log("SERVER\tREQUEST \t" + request);
			key.interestOps(SelectionKey.OP_WRITE).attach(request);
		}
	}

	@SuppressWarnings("unchecked")
	private void send() throws IOException {
		SocketChannel client = (SocketChannel) key.channel();
		String response = new String(200 + "");
		boolean reply = true;
		try {
			JSONObject request = (JSONObject) new JSONParser()
					.parse((String) key.attachment());
			final String nickname = (String) request.get("nickname");
			switch ((String) request.get("request")) {
				case "LOGIN" :
					if (ports.containsKey(nickname))
						response = 403 + "";
					else if (!db.login(nickname,
							(String) request.get("password")))
						response = 401 + "";
					else
						online.put(key.hashCode(), nickname);
					break;
				case "GET_RANKING" :
					response = db.getRanking(nickname).toString();
					break;
				case "ADD_FRIEND" :
					db.addFriend((String) request.get("nick1"),
							(String) request.get("nick2"));
					break;
				case "GET_FRIENDS" :
					response = db.getFriends(nickname).toString();
					break;
				case "REMOVE_FRIEND" :
					db.removeFriend(nickname,
							(String) request.get("friend"));
					break;
				case "UDP_PORT" :
					ports.put(nickname, (Number) request.get("port"));
					break;
				case "READY" :
					// timer start
					response = params.getK() + "";
					break;
				case "WORD" :
					response = 401 + "";
					for (Game game : games) {
						if (game.hasUser(nickname)) {
							response = game.getWord(nickname);
							if (response == null) {
								// no more words
								response = "000";
								game.userConclude();
								if (game.isConcluded()) {
									// declare the winner, updates points
									game.declareWinner();
									db.updateScore(nickname,
											game.getPoints(nickname));
									String friend = game
											.getFrienNickname(
													nickname);
									db.updateScore(friend,
											game.getPoints(friend));
									games.remove(game);
								}
							}
							break;
						}
					}
					break;
				case "CHECK_WORD" :
					String word = (String) request.get("word");
					for (Game game : games) {
						if (game.hasUser(nickname)) {
							response = game.checkTranslation(word,
									nickname) ? 200 + "" : 400 + "";
						} else {
							response = 401 + "";
						}
					}
					break;
				case "CHALLENGE" :
					// 1 - check that the users are friends
					final String friend = (String) request
							.get("friend");
					if (db.getFriends(nickname).contains(friend)
							&& ports.containsKey(friend)) {
						// check if the future is in the request object
						Future<Boolean> future;
						if (request.get("thread") == null) {
							// if it is not in the request i need to create it
							future = threadUDPExecutor
									.submit(new Callable<Boolean>() {
										public Boolean call()
												throws Exception {
											Calendar now = Calendar
													.getInstance();
											// System.out.println("ACTUAL:\t\t"
											// + now.getTimeInMillis());
											now.add(Calendar.SECOND,
													params.getT1());
											// System.out.println(
											// "TIMEOUT:\t" +
											// now.getTimeInMillis());

											byte[] request = (nickname
													+ "@"
													+ now.getTimeInMillis())
															.getBytes();
											InetAddress address = InetAddress
													.getByName(
															"localhost");
											DatagramSocket socket = new DatagramSocket();
											DatagramPacket packet = new DatagramPacket(
													request,
													request.length,
													address,
													ports.get(friend)
															.intValue());
											packet.setData(request);
											socket.send(packet);
											// 3 - wait for udp response
											byte[] buffer = new byte[1024];
											DatagramPacket receivedPacket = new DatagramPacket(
													buffer,
													buffer.length);
											System.out.println(
													"SERVER\tWaiting for response");
											socket.setSoTimeout(
													params.getT1()
															* 1000);
											try {
												socket.receive(
														receivedPacket);
												System.out.println(
														"SERVER\treceived UDP response");
												String result = new String(
														receivedPacket
																.getData(),
														0,
														receivedPacket
																.getLength());
												socket.close();
												return result.equals(
														"true");
											} catch (SocketTimeoutException e) {
												socket.close();
												return false;
											}
										}
									});
							request.put("thread", "setted");
							threadMap.put(key.hashCode(), future);
						} else {
							future = threadMap.get(key.hashCode());
						}
						if (future.isDone()) {
							// check the response and send to user A
							response = "401";
							try {
								if (future.get()) {
									response = "200";
									Game game = new Game(nickname,
											friend, params);
									games.add(game);
								}
							} catch (InterruptedException
									| ExecutionException e) {
								e.printStackTrace();
							}
						} else {
							// do not reply and set the attachment
							reply = false;
							key.attach(request.toString());
						}
					} else {
						throw new UserNotFoundException();
					}
					break;
				default :
					response = 406 + "";
			}
		} catch (ParseException | AlreadyFriendsException e) {
			// JSON sent by client is invalid
			e.printStackTrace();
			response = 406 + "";
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			response = 404 + "";
		} finally {
			if (reply) {
				logger.log("SERVER\tRESPONSE\t" + response);
				client.write(ByteBuffer.wrap(response.getBytes()));
				key.interestOps(SelectionKey.OP_READ).attach("");
			}
		}
	}

	private void register() throws IOException {
		System.out.println("Client registered");
		SocketChannel client = serverSocket.accept();
		client.configureBlocking(false);
		client.register(selector, SelectionKey.OP_READ);
	}

	public void start() {
		while (true) {
			try {
				selector.select();
				Set<SelectionKey> selectedKeys = selector
						.selectedKeys();
				Iterator<SelectionKey> iter = selectedKeys.iterator();
				while (iter.hasNext()) {
					key = iter.next();
					iter.remove();
					if (key.isAcceptable()) {
						register();
					} else if (key.isReadable()) {
						read();
					} else if (key.isWritable()) {
						send();
					}
					// TODO cambiare struttura dati nell'attachement e rimuovere
					// dalle porte l'utente
				}
			} catch (IOException e) {
				e.printStackTrace();
				key.cancel();
				try {
					key.channel().close();
				} catch (IOException close) {
					close.printStackTrace();
				}
			}
			// System.out.println("ONLINE:\t" + online);
		}
	}
}
