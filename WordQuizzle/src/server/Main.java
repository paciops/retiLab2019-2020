package server;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import database.Database;
import database.JSONDatabase;
import parameters.Parameters;

public class Main {

	public static void main(String[] args) {
		System.out.println("Server started");
		String defaultParameters = "params.json";
		int RMIport = 2000;
		if (args.length > 0)
			defaultParameters = args[0];
		try {
			Parameters params = new Parameters(defaultParameters);
			JSONDatabase database = new JSONDatabase();
			Database stub = (Database) UnicastRemoteObject
					.exportObject(database, 0);
			LocateRegistry.createRegistry(RMIport);
			Registry r = LocateRegistry.getRegistry(RMIport);

			r.rebind("DatabaseService", stub);

			// TCP
			RequestHandler requestHandler = new RequestHandler(
					params);
			requestHandler.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
