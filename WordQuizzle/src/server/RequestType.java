package server;

import java.io.Serializable;

public enum RequestType implements Serializable {
	LOGIN, LOGOUT, ADD_FRIEND, GET_FRIENDS, GET_SCORE, GET_RANKING, CHALLENGE, REMOVE_FRIEND, UDP_PORT, READY, WORD, CHECK_WORD;
}
