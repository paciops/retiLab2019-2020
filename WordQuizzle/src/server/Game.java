package server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import parameters.Parameters;
import translationService.TranslationService;

public class Game {
	private List<String> words;
	private List<String> translated;
	private TranslationService translationService;
	private Parameters parameters;

	private String firstUser;
	private String secondUser;

	private int firstUserPoints;
	private int secondUserPoints;

	private int firstUserIndex;
	private int secondUserIndex;

	private int users;

	public Game(String firstUser, String secondUser,
			Parameters params) throws IOException {
		super();
		this.translationService = new TranslationService();
		this.parameters = params;
		this.words = new ArrayList<>(
				translationService.getSetOfWords(params.getK()));
		this.translated = new ArrayList<>();
		for (String word : words) {
			translated.add(translationService.translate(word));
		}
		this.firstUser = firstUser;
		this.secondUser = secondUser;
		firstUserIndex = 0;
		secondUserIndex = 0;
		firstUserPoints = 0;
		secondUserPoints = 0;
		users = 0;
	}

	public boolean hasUser(String nick) {
		return firstUser.equals(nick) || secondUser.equals(nick);
	}

	public boolean checkTranslation(String word, String nick) {
		int index = firstUser.equals(nick)
				? firstUserIndex
				: secondUserIndex;
		String englishWord = translated.get(index - 1);
		System.out.println(
				englishWord + " è stata tradotta come " + word);
		int delta = 0;
		boolean result = englishWord.compareToIgnoreCase(word) == 0;
		if (result)
			delta += parameters.getX();
		else
			delta -= parameters.getY();
		if (firstUser.equals(nick))
			firstUserPoints += delta;
		else
			secondUserPoints += delta;
		return result;
	}

	public String getWord(String user) {
		try {
			String result;
			if (firstUser.equals(user)) {
				result = words.get(firstUserIndex);
				firstUserIndex++;
			} else {
				result = words.get(secondUserIndex);
				secondUserIndex++;
			}
			return result;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public void declareWinner() {
		int res = firstUserPoints - secondUserPoints;
		// first user has more points
		if (res > 0)
			firstUserPoints += parameters.getZ();
		// first user has more points
		else if (res < 0)
			secondUserPoints += parameters.getZ();
		// same amount of points
		// nobody gets extra points
	}

	public void userConclude() {
		users++;
	}

	public boolean isConcluded() {
		return users == 2;
	}

	public int getPoints(String nickname) {
		if (firstUser.equals(nickname))
			return firstUserPoints;
		else
			return secondUserPoints;
	}

	public String getFrienNickname(String me) {
		if (firstUser.equals(me))
			return secondUser;
		else
			return firstUser;
	}
}
