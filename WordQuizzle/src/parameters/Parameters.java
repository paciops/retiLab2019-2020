package parameters;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Parameters {
	/**
	 * K - words to choose
	 * 
	 * T1 - time lapse to accept the challenge T in seconds
	 * 
	 * T2 - duration of the challenge in seconds
	 * 
	 * X - points for correct answer (to add)
	 * 
	 * Y - points for wrong answer (to substract)
	 * 
	 * Z - points for challenge won
	 */

	private int K = 10, T1 = 30, T2 = 60, X = 2, Y = 1, Z = 3;
	/**
	 * Read the parameters from a JSON file
	 *
	 * @param path
	 *            path of the JSON file
	 * @throws IOException
	 */
	public Parameters(String path) throws IOException {
		JSONParser parser = new JSONParser();
		try {
			// TODO refactoring default value
			JSONObject json = (JSONObject) parser
					.parse(new FileReader(path));

			Long tmp = (Long) json.get("K");
			if (tmp == null) {
				System.out.println("K \tis not setted");
				tmp = (long) 10;
			}
			K = tmp.intValue();

			tmp = (Long) json.get("T1");
			if (tmp == null) {
				System.out.println("T1 \tis not setted");
				tmp = (long) 30;
			}
			T1 = tmp.intValue();

			tmp = (Long) json.get("T2");
			if (tmp == null) {
				System.out.println("T2 \tis not setted");
				tmp = (long) 60;
			}
			T2 = tmp.intValue();

			tmp = (Long) json.get("X");
			if (tmp == null) {
				System.out.println("X \tis not setted");
				tmp = (long) 2;
			}
			X = tmp.intValue();

			tmp = (Long) json.get("Y");
			if (tmp == null) {
				System.out.println("Y \tis not setted");
				tmp = (long) 1;
			}
			Y = tmp.intValue();

			tmp = (Long) json.get("Z");
			if (tmp == null) {
				System.out.println("Z \tis not setted");
				tmp = (long) 3;
			}
			Z = tmp.intValue();
		} catch (ParseException | FileNotFoundException e) {
			// File is not valid, choosing default parameters
			System.out.println(e);
			System.out.println("Setting default params");
		}
	}
	public int getK() {
		return K;
	}
	public int getT1() {
		return T1;
	}
	public int getT2() {
		return T2;
	}
	public int getX() {
		return X;
	}
	public int getY() {
		return Y;
	}
	public int getZ() {
		return Z;
	}
}
