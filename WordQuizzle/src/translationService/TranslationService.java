package translationService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TranslationService {
	private ArrayList<String> words;
	private JSONParser parser;
	public TranslationService() throws IOException {
		this("words.txt");
	}
	public TranslationService(String filePath) throws IOException {
		FileChannel inChannel = FileChannel.open(Paths.get(filePath),
				StandardOpenOption.READ);
		ByteBuffer buffer = ByteBuffer.allocateDirect(16);
		boolean cond = true;
		String tmp = "";
		while (cond) {
			cond = inChannel.read(buffer) > 0;
			buffer.flip();
			while (buffer.hasRemaining()) {
				tmp += StandardCharsets.UTF_8.decode(buffer)
						.toString();
			}
			buffer.flip();
		}
		inChannel.close();
		words = new ArrayList<>(Arrays.asList(tmp.split("\n")));
		parser = new JSONParser();
	}

	public String translate(String toTranslate) {
		try {
			URL site = new URL(
					"https://api.mymemory.translated.net/get?q="
							+ toTranslate + "&langpair=it|en");
			HttpURLConnection connection = (HttpURLConnection) site
					.openConnection();
			connection.setRequestMethod("GET");
			if (connection.getResponseCode() != 200) {
				System.err.println("Response code:\t"
						+ connection.getResponseCode());
				return null;
			}
			JSONObject response = (JSONObject) parser
					.parse(new BufferedReader(new InputStreamReader(
							connection.getInputStream())).readLine());
			return (String) ((JSONObject) response
					.get("responseData")).get("translatedText");
		} catch (IOException | ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getWord() {
		int index = (int) Math.floor(Math.random() * words.size());
		return words.get(index);
	}

	public Set<String> getSetOfWords(int n) {
		Set<String> result = new TreeSet<>();
		while (result.size() < n) {
			result.add(getWord());
		}
		return result;
	}

}
