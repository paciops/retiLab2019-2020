package translationService;

import java.io.IOException;

public class test {

	public test() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TranslationService service;
		try {
			service = new TranslationService();
			System.out.println("Translate one word:");
			String s = service.getWord();
			String translated = service.translate(s);
			print(s, translated);
			System.out.println("\n\nTranslate 50 words");
			int i = 1;
			for (String word : service.getSetOfWords(50)) {
				System.out.print(i + ")\t");
				print(word, service.translate(word));
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void print(String word, String translated) {
		if (translated == null) {
			System.err.println("Service unavailable");
		} else {
			System.out.println(word + " -> " + translated);
		}
	}
}
