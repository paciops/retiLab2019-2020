package user;

import java.util.HashSet;
import java.util.Set;

import org.json.simple.JSONObject;

public class User {
	private String nickname;
	private String password;
	private int score;
	private boolean online;
	private boolean busy;
	private Set<String> friends;
	public User(String nickname, String password) {
		this.nickname = nickname;
		this.password = password;
		this.score = 0;
		this.online = this.busy = false;
		this.friends = new HashSet<>();
	}
	public int getScore() {
		return score;
	}
	public void increaseScore(int difference) {
		this.score = score + difference;
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		this.online = online;
	}
	public boolean isBusy() {
		return busy;
	}
	public void setBusy(boolean busy) {
		this.busy = busy;
	}
	public boolean addFriend(String friend) {
		return friends.add(friend);
	}
	public Set<String> getFriend() {
		return friends;
	}
	public String getNickname() {
		return nickname;
	}
	public String getPassword() {
		return password;
	}

	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject object = new JSONObject();
		object.put("nickname", nickname);
		object.put("password", password);
		object.put("score", score);
		object.put("friends", friends);
		return object;
	}

}
