package logger;
/*
+~~~~~~+~~~~~~+~~~~~~~~~~~+
|  fg  |  bg  |  color    |
+~~~~~~+~~~~~~+~~~~~~~~~~~+
|  30  |  40  |  black    |
|  31  |  41  |  red      |
|  32  |  42  |  green    |
|  33  |  43  |  yellow   |
|  34  |  44  |  blue     |
|  35  |  45  |  magenta  |
|  36  |  46  |  cyan     |
|  37  |  47  |  white    |
|  39  |  49  |  default  |
+~~~~~~+~~~~~~+~~~~~~~~~~~+
*/
public class Logger {
	private String logColor;
	private String errorColor;
	private final String env = "DEBUG";
	private final String debug = "ON";
	public Logger(String log, String error) {
		// color must be somthing like "[37;44m"
		logColor = log;
		errorColor = error;
	}

	public Logger(String log) {
		this(log, "[31;40m");
	}

	public Logger() {
		this("[37;40m", "[31;40m");
	}

	public void log(String str) {
		if (System.getenv(env) != null
				&& debug.compareTo(System.getenv(env)) == 0) {
			System.out.print((char) 27 + logColor + str);
			clear();
		}
	}

	public void error(String str) {
		System.out.print((char) 27 + errorColor + str);
		clear();
	}

	private void clear() {
		System.out.println("\033[0m");
	}

}
