package client.game;

import java.net.URL;
import java.util.ResourceBundle;

import client.Client;
import exception.ChallengeConcludedException;
import exception.GameException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class GameController implements Initializable {
	private Client client;
	private double total;
	private int count = 0;
	@FXML
	private Text toTranslate;
	@FXML
	private TextField word;
	@FXML
	private TableView<WordTranslated> history;
	@FXML
	private TableColumn<WordTranslated, String> original;
	@FXML
	private TableColumn<WordTranslated, String> translated;
	@FXML
	private TableColumn<WordTranslated, String> correctness;
	@FXML
	private ProgressBar progressBar;

	@FXML
	private void send() {
		try {
			count++;
			progressBar.setProgress(count / total);

			history.getItems().add(new WordTranslated(
					toTranslate.getText(), word.getText(),
					client.connection.sendWord(word.getText())));

			getWord();
			word.clear();
		} catch (GameException e) {
			alertAndClose();
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		original.setCellValueFactory(
				new PropertyValueFactory<WordTranslated, String>(
						"original"));
		translated.setCellValueFactory(
				new PropertyValueFactory<WordTranslated, String>(
						"translated"));
		correctness.setCellValueFactory(
				new PropertyValueFactory<WordTranslated, String>(
						"correct" + ""));
	}

	public void alertAndClose() {
		Alert notify = new Alert(Alert.AlertType.ERROR,
				"Something went wrong, the challenge is canceled.");
		notify.show();
		client.launchHome();
	}

	public void setClient(Client client, double total, String word) {
		this.client = client;
		this.total = total;
		toTranslate.setText(word);
	}

	public void getWord() {
		try {
			toTranslate.setText(client.connection.getWord());
		} catch (GameException e) {
			alertAndClose();
		} catch (ChallengeConcludedException e) {
			endChallenge();
		}
	}

	public void endChallenge() {
		Alert notify = new Alert(Alert.AlertType.INFORMATION);
		notify.setHeaderText("The challenge is concluded");
		notify.setContentText(
				"Your points will be updated when the other player has concluded the game");
		notify.show();
		client.launchHome();
	}

}
