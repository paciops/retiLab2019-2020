package client.game;

public class WordTranslated {
	public WordTranslated(String original, String translated,
			Boolean correct) {
		super();
		this.original = original;
		this.translated = translated;
		this.correct = correct ? "✔" : "X";
	}
	private String original;
	private String translated;
	private String correct;

	public String getOriginal() {
		return original;
	}
	public void setOriginal(String original) {
		this.original = original;
	}
	public String getTranslated() {
		return translated;
	}
	public void setTranslated(String translated) {
		this.translated = translated;
	}
	public String getCorrect() {
		return correct;
	}
	public void setCorrect(String correct) {
		this.correct = correct;
	}

}
