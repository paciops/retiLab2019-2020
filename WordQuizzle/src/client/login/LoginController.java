package client.login;

import client.Client;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {
	private TextField nickname;
	private PasswordField password;
	private Client client;
	private Alert alert;

	public void setClient(Client client) {
		this.client = client;
	}

	public void setPane(Parent root) {
		nickname = (TextField) root.lookup("#nickname");
		password = (PasswordField) root.lookup("#password");
	}

	private void showError(String msg) {
		alert = new Alert(AlertType.ERROR, msg);
		alert.show();
	}

	private void showSuccess(String msg) {
		alert = new Alert(AlertType.INFORMATION, msg);
		alert.show();
	}

	@FXML
	public void login() {
		if (client.connection.login(nickname.getText(),
				password.getText())) {
			showSuccess("User logged in");
			// TODO remove
			System.out.println("User logged in");
			client.launchHome();
		} else {
			showError("Invalid username or password");
			// TODO remove
			System.out.println("Invalid username or password");
		}
	}

	@FXML
	public void register() {
		if (client.connection.registration(nickname.getText(),
				password.getText())) {
			showSuccess("User registered");
			client.launchHome();
		} else {
			showError("Invalid username or password");
		}
	}
}