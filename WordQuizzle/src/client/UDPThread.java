package client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Calendar;

import client.home.HomeController;
import javafx.application.Platform;

public class UDPThread extends Thread {
	private DatagramSocket socket;
	private HomeController homeController;
	private static boolean run;

	public UDPThread(int port, HomeController homeController)
			throws SocketException {
		socket = new DatagramSocket(port);
		this.homeController = homeController;
		run = true;
	}

	@Override
	public void run() {
		System.out.println("Thread running...");
		while (run) {
			byte[] buffer = new byte[1024];
			DatagramPacket receivedPacket = new DatagramPacket(buffer,
					buffer.length);
			System.out.println("CLIENT\tWaiting for challenges");
			try {
				socket.receive(receivedPacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			String result = new String(receivedPacket.getData(), 0,
					receivedPacket.getLength());
			String friend = result.split("@")[0];
			long timeLimit = Long.parseLong(result.split("@")[1]);
			System.out.println("CLIENT\tchallenge received");
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					System.out.println("CLIENT\task to user");
					boolean response = homeController
							.challengeAlert(friend, timeLimit);
					System.out.println(
							"CLIENT\tACCEPTED?\t" + response);
					// send response to the server
					byte[] buffer = new String(response + "")
							.getBytes();
					try {
						InetAddress address = InetAddress
								.getByName("localhost");
						DatagramPacket responsePacket = new DatagramPacket(
								buffer, buffer.length, address,
								receivedPacket.getPort());
						System.out.println("CLIENT\tsend response");
						socket.send(responsePacket);
						System.out.println("CLIENT\tresponse sent");
						run = !response;
						// if true open the game GUI
						if (response) {
							homeController.launchGame();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			Calendar now = Calendar.getInstance();
			try {
				Thread.sleep(timeLimit - now.getTimeInMillis());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Thread closing");
	}

	public void close() {
		socket.close();
	}
}
