package client.home;

import java.net.SocketException;
import java.net.URL;
import java.util.Calendar;
import java.util.Optional;
import java.util.ResourceBundle;

import client.Client;
import client.UDPThread;
import client.User;
import exception.ChallengeConcludedException;
import exception.GameException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;

public class HomeController implements Initializable {
	private Client client;
	@FXML
	private TableView<User> usersTable;
	@FXML
	private TableColumn<User, String> nicknameColumn;
	@FXML
	private TableColumn<User, Integer> scoreColumn;

	@FXML
	private MenuButton removeFriendMenu;
	@FXML
	private MenuButton challengeMenu;

	private UDPThread thread;

	private EventHandler<ActionEvent> challengeFriend = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent e) {
			String friend = ((MenuItem) e.getSource()).getText();
			if (client.connection.challengeFriend(friend)) {
				launchGame();
			} else {
				Alert notify = new Alert(AlertType.ERROR,
						"Cannot compete with " + friend + " now.");
				notify.show();
			}
		}
	};

	private EventHandler<ActionEvent> removeFriend = new EventHandler<ActionEvent>() {
		public void handle(ActionEvent e) {
			String friendNickname = ((MenuItem) e.getSource())
					.getText();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Remove friend");
			String s = "Confirm to remove " + friendNickname
					+ " from your friends.";
			alert.setContentText(s);
			Optional<ButtonType> result = alert.showAndWait();
			if ((result.isPresent())
					&& (result.get() == ButtonType.OK)) {
				client.connection.removeFriend(friendNickname);
				populate();
			}
		}
	};

	private void populate() {
		usersTable.setItems(client.connection.getRanking());
		for (String friend : client.connection.getFriends()) {
			final MenuItem removeItem = new MenuItem(friend);
			removeItem.setOnAction(removeFriend);
			removeFriendMenu.getItems().add(removeItem);

			final MenuItem challengeItem = new MenuItem(friend);
			challengeItem.setOnAction(challengeFriend);
			challengeMenu.getItems().add(challengeItem);
		}
	}

	public boolean challengeAlert(final String friend,
			final long timeLimit) {
		Alert notify = new Alert(Alert.AlertType.CONFIRMATION);
		notify.setTitle("Challenge request");
		notify.setHeaderText(friend + " ask you to join a new match");
		notify.setContentText("What do you want to do?");
		ButtonType accept = new ButtonType("Accept");
		ButtonType deny = new ButtonType("Deny");
		notify.getButtonTypes().setAll(accept, deny);
		notify.getButtonTypes().add(ButtonType.CANCEL);
		notify.hide();
		notify.getButtonTypes().remove(ButtonType.CANCEL);

		Optional<ButtonType> result = notify.showAndWait();
		Calendar now = Calendar.getInstance();
		System.out.println("NOW:\t" + now.getTimeInMillis());
		System.out.println("LIMIT:\t" + timeLimit);
		if (now.getTimeInMillis() >= timeLimit) {
			notify = new Alert(Alert.AlertType.ERROR, "Timeout");
			notify.show();
		}
		return result.get() == accept
				&& now.getTimeInMillis() <= timeLimit;
	}

	private void clear() {
		usersTable.setItems(null);
		removeFriendMenu.getItems().clear();
		challengeMenu.getItems().clear();
	}

	public void setClient(Client client) {
		this.client = client;
		this.refesh();
	}

	public void startThread(int port) {
		try {
			client.connection.sendUDPPort(port);
			thread = new UDPThread(port, this);
			thread.start();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			System.out
					.println("Porta già in uso?\t" + e.getMessage());
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		nicknameColumn.setCellValueFactory(
				new PropertyValueFactory<User, String>("nickname"));
		scoreColumn.setCellValueFactory(
				new PropertyValueFactory<User, Integer>("score"));
	}

	@FXML
	private void addFriend() {
		TextInputDialog dialog = new TextInputDialog("nickname");
		dialog.setTitle("Add a friend");
		dialog.setHeaderText("Add a friend");
		dialog.setContentText("Please enter your friend's nickname:");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			int response = client.connection.addFriend(result.get());
			Alert notify = new Alert(null);
			if (response == 200) {
				notify.setAlertType(AlertType.INFORMATION);
				notify.setContentText(
						result.get() + " is your friend.");
			} else if (response == 404) {
				notify.setAlertType(AlertType.ERROR);
				notify.setContentText(
						result.get() + " is not registered.");
			} else {
				notify.setAlertType(AlertType.ERROR);
				notify.setContentText(
						"Response from server: " + response);
			}
			notify.show();
			populate();
		}

	}

	@FXML
	private void refesh() {
		clear();
		populate();
	}

	@FXML
	private void logout() {
		client.launchLogin();
		thread.close();
		thread.interrupt();
	}

	public void launchGame() {
		try {
			client.launchGame(client.connection.readyToPlay(),
					client.connection.getWord());
		} catch (GameException | ChallengeConcludedException e) {
			new Alert(Alert.AlertType.ERROR,
					"Something went wrong, the challenge is canceled.")
							.show();
		}
	}
}
