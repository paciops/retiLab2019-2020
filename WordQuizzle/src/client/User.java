package client;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class User {
	private SimpleStringProperty nickname;
	private SimpleIntegerProperty score;

	public User(String nickname, Integer score) {
		this.nickname = new SimpleStringProperty(nickname);
		this.score = new SimpleIntegerProperty(score);
	}

	public String getNickname() {
		return nickname.get();
	}
	public int getScore() {
		return score.get();
	}
	public void setNickname(String nickname) {
		this.nickname.set(nickname);
	}
	public void setScore(int score) {
		this.score.set(score);
	}
}
