package client.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import client.User;
import database.Database;
import exception.ChallengeConcludedException;
import exception.GameException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logger.Logger;
import server.RequestType;

@SuppressWarnings("unchecked")
public class Connection {

	private static SocketChannel client;
	private int RMIport;
	private int TCPport;
	private ByteBuffer buffer;
	private JSONParser parser;
	private String nickname;
	private Logger logger;

	public Connection() throws IOException {
		RMIport = 2000;
		TCPport = 5454;
		parser = new JSONParser();
		logger = new Logger("[36;47m");
		buffer = ByteBuffer.allocate(2048);
		client = SocketChannel
				.open(new InetSocketAddress("localhost", TCPport));
	}

	public boolean registration(String nickname, String password) {
		try {
			Registry registry = LocateRegistry.getRegistry(RMIport);
			Remote remoteObject = registry.lookup("DatabaseService");
			Database serverObject = (Database) remoteObject;
			final boolean register = serverObject.register(nickname,
					password);
			if (register)
				this.nickname = nickname;
			return register;
		} catch (RemoteException | NotBoundException e) {
			System.err.println(e);
			return false;
		}
	}

	public boolean login(String nickname, String password) {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.LOGIN.name());
		request.put("nickname", nickname);
		request.put("password", password);
		try {
			int response = Integer
					.parseInt(sendRequest(request.toString()));
			if (response == 200)
				this.nickname = nickname;
			return response == 200;
		} catch (IOException | NumberFormatException e) {
			System.err.println(e);
			return false;
		}
	}

	private String sendRequest(String request) throws IOException {
		String response;
		buffer = ByteBuffer.wrap(request.getBytes());
		client.write(buffer);
		// TODO not a good solution, maybe I will implement a new one
		final int size = 1024 * 1024;
		buffer = ByteBuffer.allocate(size);
		client.read(buffer);
		response = new String(buffer.array()).trim();

		logger.log("CLIENT\tREQUEST:\t" + request);
		logger.log("CLIENT\tRESPONSE:\t" + response);

		return response;
	}

	public ObservableList<User> getRanking() {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.GET_RANKING.name());
		request.put("nickname", nickname);
		ObservableList<User> list = FXCollections
				.observableArrayList();
		try {
			JSONArray json = (JSONArray) parser
					.parse(sendRequest(request.toString()));
			json.forEach((obj) -> {
				JSONObject user = (JSONObject) obj;
				list.add(new User((String) user.get("nickname"),
						((Long) user.get("score")).intValue()));
			});
			return list;
		} catch (IOException | ParseException
				| ClassCastException e) {
			System.err.println(e);
			return list;
		}
	}

	public int addFriend(String friend) {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.ADD_FRIEND.name());
		request.put("nick1", nickname);
		request.put("nick2", friend);
		try {
			return Integer.parseInt(sendRequest(request.toString()));
		} catch (IOException | NumberFormatException e) {
			System.err.println(e);
			return 0;
		}
	}

	public List<String> getFriends() {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.GET_FRIENDS.name());
		request.put("nickname", nickname);
		try {
			JSONArray friends = (JSONArray) parser
					.parse(sendRequest(request.toString()));

			return friends;
		} catch (IOException | ParseException
				| ClassCastException e) {
			System.err.println(e);
			return new ArrayList<>();
		}
	}

	public void removeFriend(String friend) {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.REMOVE_FRIEND.name());
		request.put("nickname", nickname);
		request.put("friend", friend);
		try {
			sendRequest(request.toString());
		} catch (IOException | ClassCastException e) {
			System.err.println(e);
		}
	}

	public boolean challengeFriend(String friend) {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.CHALLENGE.name());
		request.put("nickname", nickname);
		request.put("friend", friend);
		try {
			final int response = Integer
					.parseInt(sendRequest(request.toString()));
			return response == 200;
		} catch (IOException | ClassCastException e) {
			System.err.println(e);
			return false;
		}
	}

	public void sendUDPPort(int port) {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.UDP_PORT.name());
		request.put("nickname", nickname);
		request.put("port", port);
		try {
			sendRequest(request.toString());
		} catch (IOException | ClassCastException e) {
			System.err.println(e);
		}
	}

	public boolean sendWord(String word) throws GameException {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.CHECK_WORD.name());
		request.put("nickname", nickname);
		request.put("word", word);
		try {
			int result = Integer
					.parseInt(sendRequest(request.toString()));
			if (result == 200)
				return true;
			else if (result == 400)
				return false;
			else
				throw new GameException();
		} catch (IOException | ClassCastException e) {
			throw new GameException();
		}
	}

	public int readyToPlay() throws GameException {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.READY.name());
		request.put("nickname", nickname);
		try {
			return Integer.parseInt(sendRequest(request.toString()));
		} catch (IOException | ClassCastException e) {
			throw new GameException();
		}
	}

	public String getWord()
			throws GameException, ChallengeConcludedException {
		JSONObject request = new JSONObject();
		request.put("request", RequestType.WORD.name());
		request.put("nickname", nickname);
		try {
			String result = sendRequest(request.toString());
			if (result.compareTo("401") == 0)
				throw new GameException();
			else if (result.compareTo("000") == 0)
				throw new ChallengeConcludedException();
			return result;
		} catch (IOException | ClassCastException e) {
			throw new GameException();
		}
	}

	public void close() {
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
