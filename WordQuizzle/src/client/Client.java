package client;

import java.io.IOException;
import java.util.List;

import client.connection.Connection;
import client.game.GameController;
import client.home.HomeController;
import client.login.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import logger.Logger;

public class Client extends Application {
	private Stage stage;
	public Connection connection;
	private int portUDP;
	private Logger logger;
	private FXMLLoader loader;

	@Override
	public void start(Stage primaryStage) {
		portUDP = 9999;
		stage = primaryStage;
		logger = new Logger();
		List<String> params = getParameters().getRaw();
		for (int i = 0; i < params.size(); i++) {
			if (params.get(i).compareTo("--port") == 0) {
				i++;
				portUDP = Integer.parseInt(params.get(i));
			}
		}
		logger.log("UDP port: " + portUDP);
		launchLogin();
	}

	public void launchLogin() {
		try {
			loader = new FXMLLoader(
					getClass().getResource("login/main.fxml"));
			Parent root = loader.load();
			Scene scene = new Scene(root);
			LoginController loginController = loader.getController();
			this.connection = new Connection();
			stage.setTitle("World Quizzle");
			stage.setScene(scene);
			stage.show();
			stage.setOnCloseRequest(arg0 -> {
				logger.log("Closing all connections.");
				connection.close();
				System.exit(0);
			});
			loginController.setPane(root);
			loginController.setClient(this);
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR,
					"Server is not running");
			alert.show();
		}
	}

	public void launchHome() {
		try {
			loader = new FXMLLoader(
					getClass().getResource("home/main.fxml"));
			Parent root = loader.load();
			HomeController homeController = loader.getController();
			homeController.setClient(this);
			homeController.startThread(portUDP);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setOnCloseRequest(arg0 -> {
				logger.log("Closing client.");
				connection.close();
				System.exit(0);
			});
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void launchGame(double total, String word) {
		try {
			loader = new FXMLLoader(
					getClass().getResource("game/main.fxml"));
			Parent root = loader.load();
			GameController gameController = loader.getController();
			gameController.setClient(this, total, word);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
