package exception;

public class ChallengeConcludedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4040878057262349550L;

	public ChallengeConcludedException() {
		// TODO Auto-generated constructor stub
	}

	public ChallengeConcludedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ChallengeConcludedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ChallengeConcludedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ChallengeConcludedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
