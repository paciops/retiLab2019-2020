package exception;

public class AlreadyFriendsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -191049326516457166L;

	public AlreadyFriendsException() {
		// TODO Auto-generated constructor stub
	}

	public AlreadyFriendsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AlreadyFriendsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AlreadyFriendsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AlreadyFriendsException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
