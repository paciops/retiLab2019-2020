package exception;

public class GameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4371592183746447483L;

	public GameException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GameException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GameException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GameException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GameException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
