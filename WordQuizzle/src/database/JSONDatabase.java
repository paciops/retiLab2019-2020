package database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import exception.AlreadyFriendsException;
import exception.UserNotFoundException;
import user.User;

@SuppressWarnings("unchecked")
public class JSONDatabase implements Database {
	private JSONParser parser;
	private String filePath;

	public JSONDatabase() {
		this("users.json");
	}

	public JSONDatabase(String filePath) {
		if (filePath == null) {
			System.err.println("Users json file path is NULL");
			System.exit(2);
		}
		this.filePath = filePath;
		parser = new JSONParser();
		JSONArray users;
		try {
			users = (JSONArray) parser
					.parse(new FileReader(filePath));
			System.out.println("Using " + filePath);
		} catch (FileNotFoundException e) {
			System.err.println(e);
			users = new JSONArray();
			System.out.println("File " + filePath
					+ " not found. Creating a new one.");
			writeJSON(users);
		} catch (IOException e) {
			// TODO manage exception
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO manage exception
			e.printStackTrace();
		}
	}

	private void writeJSON(JSONArray users) {
		try (FileWriter file = new FileWriter(filePath)) {
			file.write(users.toString());
			file.flush();
		} catch (IOException ex) {
			System.err.println("Error when writing a JSON file" + ex);
			System.exit(3);
		}

	}

	private JSONArray readJSON() {
		try {
			return (JSONArray) parser.parse(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			System.err.println(e);
			System.exit(4);
		} catch (IOException | ParseException e) {
			System.err.println(e);
		}
		return new JSONArray();
	}

	private synchronized void addUser(User user) {
		final JSONArray users = readJSON();
		users.add(user.toJSON());
		writeJSON(users);
	}

	private synchronized void updateUser(JSONObject user) {
		final JSONArray users = readJSON();
		for (Object object : users) {
			JSONObject u = (JSONObject) object;
			if (((String) u.get("nickname"))
					.compareTo((String) user.get("nickname")) == 0) {
				u.put("score", user.get("score"));
				u.put("friends", user.get("friends"));
				writeJSON(users);
			}
		}
	}

	private synchronized JSONObject getUser(String nickname)
			throws UserNotFoundException {
		if (nickname == null)
			throw new NullPointerException("NICKNAME IS NULL");
		JSONArray users = readJSON();
		for (Object userTmp : users) {
			JSONObject user = (JSONObject) userTmp;
			if (nickname
					.compareTo((String) user.get("nickname")) == 0)
				return (JSONObject) user;

		}
		throw new UserNotFoundException(nickname);
	}

	/*
	 * private synchronized boolean userIsIn(String nickname) { try {
	 * getUser(nickname); return true; } catch (UserNotFoundException e) {
	 * return false; } }
	 */
	@Override
	public boolean register(String nickname, String password)
			throws RemoteException {
		// check if the nickname is already in the platform
		try {
			getUser(nickname);
			return false;
		} catch (UserNotFoundException e) {
			// nickname is not in the database
			// check if the password is correct
			// if it is different from empty string
			// user can be added
			if (password.trim().compareTo("") == 0)
				return false;
			// TODO hash the password
			User user = new User(nickname, password);
			addUser(user);
			return true;
		}
	}

	/**
	 * @param nickname
	 *            nickname of the user
	 * @param password
	 *            password of the user
	 * @return true is login is successful, else otherwise
	 */
	public boolean login(String nickname, String password) {
		JSONObject user;
		try {
			user = getUser(nickname);
			return password
					.compareTo((String) user.get("password")) == 0;
		} catch (UserNotFoundException e) {
			return false;
		}
	}

	/**
	 * @param nickname
	 *            nickname of the user
	 * @param password
	 *            password of the user
	 * @return true is logout is successful, else otherwise
	 */
	public boolean logout(String nickname, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @param myNickname
	 *            nickname of the user
	 * @param friendNickname
	 *            nickname of the friend
	 * @throws AlreadyFriendsException
	 *             if the users are already friends
	 * @throws UserNotFoundException
	 *             if one of the users is not in the platform
	 */
	public void addFriend(String myNickname, String friendNickname)
			throws AlreadyFriendsException, UserNotFoundException {
		// check if friendNickname exists
		JSONArray user = getFriends(myNickname);
		JSONArray friend = getFriends(friendNickname);
		if (user.contains(friendNickname)
				&& friend.contains(myNickname))
			throw new AlreadyFriendsException(myNickname + " and "
					+ friendNickname + " are already friends.");
		JSONObject tmp = getUser(myNickname);
		((JSONArray) tmp.get("friends")).add(friendNickname);
		updateUser(tmp);
		tmp = getUser(friendNickname);
		((JSONArray) tmp.get("friends")).add(myNickname);
		updateUser(tmp);
	}

	/**
	 * @param nickname
	 *            nickname of the user
	 * @return a JSON that contains the friends of the user
	 * @throws UserNotFoundException
	 *             if user is not found
	 */
	public JSONArray getFriends(String nickname)
			throws UserNotFoundException {
		JSONObject user = getUser(nickname);
		return (JSONArray) user.get("friends");
	}
	/**
	 * @param nickname
	 *            nickname of the user
	 * @return the score of the user
	 * @throws UserNotFoundException
	 *             if the user is not in the platform
	 */
	public int getScore(String nickname)
			throws UserNotFoundException {
		JSONObject user = getUser(nickname);
		return ((Long) user.get("score")).intValue();
	}

	public void updateScore(String nickname, int delta)
			throws UserNotFoundException {
		JSONObject tmp = getUser(nickname);
		Long score = (Long) tmp.get("score");
		score += delta;
		tmp.put("score", score);
		updateUser(tmp);
	}

	/**
	 * @param nickname
	 *            nickname of the user
	 * @return a JSON that contains the ranking of the users and his friends
	 * @throws UserNotFoundException
	 */
	public JSONArray getRanking(String nickname)
			throws UserNotFoundException {
		JSONArray friends = getFriends(nickname);
		JSONArray ranking = new JSONArray();
		friends.add(nickname);
		for (Object userTmp : friends) {
			String user = (String) userTmp;
			int score = getScore(user);
			JSONObject obj = new JSONObject();
			obj.put("nickname", user);
			obj.put("score", score);
			ranking.add(obj);
		}
		return ranking;
	}

	public void removeFriend(String myNickname, String friendNickname)
			throws UserNotFoundException {
		// check if friendNickname exists
		JSONArray user = getFriends(myNickname);
		JSONArray friend = getFriends(friendNickname);
		if (!user.contains(friendNickname)
				|| !friend.contains(myNickname))
			throw new UserNotFoundException(myNickname + " and "
					+ friendNickname + " are not friends.");
		JSONObject tmp = getUser(myNickname);
		((JSONArray) tmp.get("friends")).remove(friendNickname);
		updateUser(tmp);
		tmp = getUser(friendNickname);
		((JSONArray) tmp.get("friends")).remove(myNickname);
		updateUser(tmp);
	}
}
