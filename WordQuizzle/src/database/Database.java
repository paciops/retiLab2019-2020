package database;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Database extends Remote {
	/**
	 * Register the user on the platform
	 *
	 * @param nickname
	 *            nickname of the user
	 * @param password
	 *            password of the user
	 * @throws RemoteException
	 * @return true is registration is successful, else otherwise
	 */
	public boolean register(String nickname, String password)
			throws RemoteException;
}
