import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

class Test{
	public static void main() {
		ExecutorService executor = Executors.newCachedThreadPool();
		List<Future> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			Future future = executor.submit(new Callable(){
				public Double call() throws Exception {
					String threadName = Thread.currentThread().getName();
					System.out.println("Executor con un solo Thread utilizzando Callable: " + threadName);
					TimeUnit.SECONDS.sleep(10);
					return Math.random()*100;
				}
			});
			list.add(future);
		}
		for (Future future : list) {
			try {
				System.out.println("Il Thread ha completato il suo lavoro: "+future.get());
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				e1.printStackTrace();
			}			
		}
	}
}
