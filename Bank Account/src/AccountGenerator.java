import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.GregorianCalendar;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/*
 * classe che crea il file json
 * prende 3 parametri opzionali in input
 * il primo è il nome del file
 * il secondo è il numero di conti
 * il terzo è il numero di transazioni per conto
 * la struttura del json generato è la seguente
 * 
 * [{
 * 	"name": "riccardo pacioni",
 * 	"transactions": [
 * 		{"date": "2018-08-09", "causal": "F24"}
 * 	]
 * }]
 */
public class AccountGenerator {
	final static String[] surnames = {"Abagnale", "Abagnali",
			"Abatangelo", "Abatantuono", "Abate", "Abatecola",
			"Abategiovanni", "Abati", "Abaticola", "Abba", "Abba'",
			"Abbadelli", "Abbagnale", "Abbascia", "Abbatangelo",
			"Abbatantuono", "Abbate", "Abbatecola", "Abbatelli",
			"Abbaticola", "Abbiati", "Abbiento", "Abbondanza",
			"Abbondanzieri", "Abbondanzio", "Abbondi", "Abbondio",
			"Abbrescia"};
	final static String[] names = {"decia", "decima", "decimina",
			"decimino", "decimo", "decio", "dedalo", "defendente",
			"defendi", "defendina", "defendo", "degna", "deianira",
			"delcisa", "delciso", "deledda", "delfa"};

	final static String[] causal = {"Bonifico", "Accredito",
			"Bollettino", "F24", "PagoBancomat"};

	// restituisce una stringa casuale dall'array passato
	private static String getRandomString(String[] list) {
		int index = (int) (Math.random() * list.length);
		return list[index];
	}

	private static int randBetween(int start, int end) {
		return start
				+ (int) Math.round(Math.random() * (end - start));
	}

	// genera una data a caso dal 2017 al 2019
	// in formato YYYY-MM-DD
	private static String getRandomDate() {
		GregorianCalendar gc = new GregorianCalendar();

		int year = randBetween(2017, 2019);

		gc.set(YEAR, year);

		int dayOfYear = randBetween(1,
				gc.getActualMaximum(DAY_OF_YEAR));

		gc.set(DAY_OF_YEAR, dayOfYear);

		return gc.get(YEAR) + "-" + (gc.get(MONTH) + 1) + "-"
				+ gc.get(DAY_OF_MONTH);
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		int nAccount = 10;
		int nTransactions = 5;
		String file = "accounts.json";
		if (args.length >= 1)
			file = args[0];
		if (args.length >= 2)
			nAccount = Integer.parseInt(args[1]);
		if (args.length >= 3)
			nTransactions = Integer.parseInt(args[2]);

		// se non vengono passati paramentri uso
		// dei dati di default

		FileChannel outChannel = null;
		// prendo il path del file passto
		Path path = Paths.get(file);
		try {
			// lo cancello se esiste e poi lo creo
			// così facendo evito di sovrascrivere
			// sopra dati già presenti
			Files.deleteIfExists(path);
			Files.createFile(path);
		} catch (IOException e1) {
			System.err.println("File already created");
		}

		try {
			// apro il file in scrittura
			outChannel = FileChannel.open(path,
					StandardOpenOption.WRITE);
			// scrivo sul channel la prima parentesi quadra del json
			outChannel.write(ByteBuffer.wrap("[".getBytes()));
			// inizio ad inserire nel channel i vari oggetti
			// in questa maniera riesco a scrivere grosse quantità di dati
			for (int i = 0; i < nAccount; i++) {
				JSONObject obj = new JSONObject();
				obj.put("name", getRandomString(names) + " "
						+ getRandomString(surnames));
				JSONArray transactions = new JSONArray();
				// aggiungo le transazioni all'oggetto
				for (int k = 0; k < nTransactions; k++) {
					JSONObject tran = new JSONObject();
					tran.put("date", getRandomDate());
					tran.put("causal", getRandomString(causal));
					transactions.add(tran);
				}
				obj.put("transactions", transactions);
				String toBytes = obj.toJSONString();
				// scrivo la virgola se non sono all'ultimo oggetto
				if (i != nAccount - 1)
					toBytes += ",";
				ByteBuffer buffer = ByteBuffer
						.wrap(toBytes.getBytes());
				while (buffer.hasRemaining()) {
					outChannel.write(buffer);
				}
			}
			// scrivo la quadra finale
			outChannel.write(ByteBuffer.wrap("]".getBytes()));
			System.out.println("file generated");
			outChannel.close();
		} catch (OutOfMemoryError e) {
			System.err.println("out of memory");
			System.exit(-1);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			System.exit(-2);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-3);
		}
	}
}