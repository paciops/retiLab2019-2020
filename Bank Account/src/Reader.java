import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Reader extends Thread {
	private SharedData data;
	private String path;
	public Reader(SharedData data, String path) {
		this.data = data;
		this.path = path;
	}

	public void run() {
		// per "monitorare le performance" eseguo un po'
		// di stampe per sapere quanto tempo impiego
		// nelle varie fasi
		long start = System.currentTimeMillis();
		JSONArray accounts = new JSONArray();
		String toParse = readJSON(path);
		System.out.println("Lettura JSON:\t"
				+ ((System.currentTimeMillis() - start) / 1000.0)
				+ " s");
		start = System.currentTimeMillis();
		JSONParser jsonParser = new JSONParser();
		try {
			accounts = (JSONArray) jsonParser.parse(toParse);
		} catch (ParseException e1) {
			System.err.println("Parsing fallito");
			System.exit(-3);
		}
		System.out.println("Parsing JSON\t"
				+ ((System.currentTimeMillis() - start) / 1000.0)
				+ " s");
		start = System.currentTimeMillis();
		while (!accounts.isEmpty()) {
			// ciclo sulla lista di conti correnti e
			// li passo alla classe condivisa
			// rimuovo sempre in testa
			try {
				JSONObject obj = (JSONObject) accounts.remove(0);
				data.put(obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Passagio JSONObject:\t"
				+ ((System.currentTimeMillis() - start) / 1000.0)
				+ " s");
		data.terminate();
	}

	private String readJSON(String path) {
		try {
			// leggo il file
			FileChannel inChannel = FileChannel.open(Paths.get(path),
					StandardOpenOption.READ);
			long lastPrint = System.currentTimeMillis() + 1000;
			int cont = 0;
			// il buffer ha dimensioni 10MB
			ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024 * 10);
			boolean stop = false;
			String value = "";
			while (!stop) {
				// questo pezzo di codice serve a stampare
				// la percentuale di file letto
				if (cont != 0 && lastPrint
						- System.currentTimeMillis() >= -1000) {
					System.out.print("\033[1A\r\033[J");
					System.out.printf("%.2f %% \n",
							(float) 100 * cont / inChannel.size());
				} else {
					lastPrint = System.currentTimeMillis();
				}

				int bytesRead = inChannel.read(buffer);
				// mi fermo quando non leggo più byte dal channel
				stop = bytesRead == -1;
				buffer.flip();
				while (buffer.hasRemaining()) {
					value += StandardCharsets.UTF_8.decode(buffer)
							.toString();
					// immagazzino tutto quello che leggo dal file
					// in una stringa che poi andrò a parsare
					cont += buffer.position();
				}
				buffer.clear();
			}
			inChannel.close();
			return value;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-2);
		}
		return null;
	}

}
