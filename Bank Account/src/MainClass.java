/*
 * classe main del progetto bank account
 * prende come primo argomento il nome del file
 * se non è passato prende di default accounts.json
 */

public class MainClass {

	public static void main(String[] args) {
		SharedData data = new SharedData();
		String file = "accounts.json";
		if (args.length >= 1)
			file = args[0];
		Thread reader = new Thread(new Reader(data, file));
		reader.start();
		Pool pool = new Pool(4, 10);
		try {
			// ciclo finchè i dati (ovvero i conti correnti)
			// non sono finiti
			while (data.isNotOver()) {
				if (!pool.isFull() && data.isNotOver()) {
					pool.execute(new Printer(data));
				}
			}
		} finally {
			pool.shutdown();
			// la shutdown è necessaria per sapere se
			// tutti i thread hanno finito di contare
			System.out.println(data.getData());
			System.exit(0);
		}
	}

}
