import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
/*
 * classe condivisa che contiene due strutture dati
 * una che immagazzina i conti correnti presi dal json
 * e l'altra che conta le occorrenze
 */

public class SharedData {
	private JSONArray data;
	private Lock dataLock;
	private final Condition dataCondition;
	private ConcurrentHashMap<String, Integer> map;
	private boolean ended = false;
	public SharedData() {
		this.data = new JSONArray();
		this.dataLock = new ReentrantLock();
		this.dataCondition = dataLock.newCondition();
		this.map = new ConcurrentHashMap<String, Integer>();
	}

	public JSONObject getAccount() {
		dataLock.lock();
		try {
			while (data.isEmpty() && !ended)
				dataCondition.await();
			return (JSONObject) data.remove(0);
		} catch (InterruptedException | IndexOutOfBoundsException e) {
			return null;
		} finally {
			dataLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public void put(JSONObject obj) throws Exception {
		dataLock.lock();
		try {
			boolean empty = data.isEmpty();
			if (!data.add((Object) obj)) {
				throw new Exception(obj + "Can not add " + obj);
			}
			if (empty)
				dataCondition.signal();
		} finally {
			dataLock.unlock();
		}
	}

	public boolean isNotOver() {
		dataLock.lock();
		try {
			return !data.isEmpty() || !ended;
		} finally {
			dataLock.unlock();
		}
	}

	public void terminate() {
		dataLock.lock();
		try {
			ended = true;
			dataCondition.signalAll();
		} finally {
			dataLock.unlock();
		}
	}

	public void increment(String causal) {
		dataLock.lock();
		try {
			Integer value = map.get(causal);
			if (value == null)
				map.put(causal, 1);
			else
				map.put(causal, value + 1);
		} finally {
			dataLock.unlock();
		}
	}

	public String getData() {
		dataLock.lock();
		try {
			return map.toString();
		} finally {
			dataLock.unlock();
		}
	}
}
