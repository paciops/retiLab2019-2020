
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Pool {
	private ThreadPoolExecutor executor;
	private ArrayBlockingQueue<Runnable> list;
	public Pool(int nThread, int capacity) {
		list = new ArrayBlockingQueue<Runnable>(capacity, true);
		executor = new ThreadPoolExecutor(nThread, nThread, 60L,
				TimeUnit.SECONDS, list);
	}

	public void execute(Printer printer) {
		executor.execute(printer);
	}

	public boolean isFull() {
		return list.remainingCapacity() == 0;
	}

	public void shutdown() {
		executor.shutdown();
		try {
			if (executor.awaitTermination(5L, TimeUnit.SECONDS)) {
				System.out.println("Pool spento");
			} else {
				System.err.println("Pool ucciso");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
