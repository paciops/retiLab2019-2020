import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Printer implements Runnable {
	private SharedData data;
	private JSONArray transactions;

	public Printer(SharedData data) {
		this.data = data;
	}

	@Override
	public void run() {
		// prendo un contocorrent
		JSONObject array = (JSONObject) data.getAccount();
		if (array == null)
			return;
		// se ritorna null sono finiti i conti nella lista
		// altrimenti prendo le transazioni di quel conto
		transactions = (JSONArray) array.get("transactions");
		for (Object elem : transactions) {
			JSONObject transaction = (JSONObject) elem;
			String causal = (String) transaction.get("causal");
			// controllo la causale e incremento il valore
			// nell'hash map che ha come chiave la causale
			data.increment(causal);
		}
	}

}
