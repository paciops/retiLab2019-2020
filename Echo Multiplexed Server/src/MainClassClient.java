import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class MainClassClient {

	public static void main(String[] args) {
		// la porta come primo argomento
		// il messaggio come secondo argomento
		// sono opzionali
		try {
			int port;
			if (args.length > 0)
				port = Integer.parseInt(args[0]);
			else
				port = 8080;
			byte[] toSend;
			if (args.length > 1)
				toSend = args[1].getBytes();
			else
				toSend = "prova".getBytes();
			System.out.println("CLIENT START");
			// Mi connetto al server su localhost:8080
			SocketAddress address = new InetSocketAddress(port);
			SocketChannel client = SocketChannel.open(address);
			client.write(ByteBuffer.wrap(toSend));
			System.out.println("SENT:\t" + new String(toSend));
			ByteBuffer buffer = ByteBuffer.allocate(2);
			// Chiudo la connessione in scrittura senza chiudere il canale
			client = client.shutdownOutput();
			String result = "";
			boolean cond = true;
			while (cond) {
				cond = client.read(buffer) > 0;
				buffer.flip();
				while (buffer.hasRemaining())
					result += StandardCharsets.UTF_8.decode(buffer).toString();
				buffer.clear();
			}
			System.out.printf("RECIVED: %s\n", result);
		} catch (NumberFormatException e) {
			System.err.println("Wrong parameter arg0 -> port, arg1 -> message");
			System.exit(-1);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(-2);
		}
	}

}
