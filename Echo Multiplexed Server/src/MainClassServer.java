import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

// il primo argomento può essere la porta
public class MainClassServer {
	public static int DEFAULT_PORT = 8080;
	public static int CAPACITY = 1024;

	public static void main(String[] args) {
		int port;
		if (args.length > 0)
			port = Integer.parseInt(args[0]);
		else
			port = DEFAULT_PORT;
		System.out.println("Port:\t" + port);
		ServerSocketChannel serverChannel;
		Selector selector = null;
		try {
			serverChannel = ServerSocketChannel.open();
			ServerSocket serverSocket = serverChannel.socket();
			// Lega alla porta specificata e all'indirizzo wildcard
			// un oggetto socketAddress
			InetSocketAddress address = new InetSocketAddress(port);
			serverSocket.bind(address);
			// il server non si blocca se non ci sono 
			// richieste di connessione, può servire altri client già connessi
			serverChannel.configureBlocking(false);
			// il client richiede una connessione al server
			// creo il selettore e lo registro sul server
			selector = Selector.open();
			serverChannel.register(selector, SelectionKey.OP_ACCEPT);
		} catch (IOException e) {
			System.err.printf("ERRORE:\t%s\n", e.toString());
			System.exit(-1);
		}
		// ciclo all'infinito
		while (true && selector != null) {
			try {
				// seleziono i canali pronti per almeno una delle operazioni 
				// di I/O tra quelli registrati con quel selettore
				selector.select();
			} catch (IOException ioe) {
				System.err.println("IOE:\n\t" + ioe.getMessage());
			}
			/*
			 * SelectionKeys dei canali identificati come pronti, nell'ultima operazione di
			 * selezione, per eseguire almeno una operazione contenuta nell'interest set
			 * della chiave,
			 */
			Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
			while (iterator.hasNext()) {
				SelectionKey key = iterator.next();
				iterator.remove();
				try {
					if (key.isAcceptable()) {
						// a connection was accepted by a
						// ServerSocketChannel.
						ServerSocketChannel server = (ServerSocketChannel) key.channel();
						SocketChannel client = server.accept();
						client.configureBlocking(false);
						client.register(selector, SelectionKey.OP_READ);
						System.out.println(client.socket().getInetAddress() + "\t ACCEPTED");
					} else if (key.isConnectable()) {
						// a connection was established with a remote server
						System.out.println("is connectable");
					} else if (key.isReadable()) {
						// a channel is ready for reading
						SocketChannel channel = (SocketChannel) key.channel();
						// il buffere è di dimensione 1 per testare come si comporta il server in casi estremi
						ByteBuffer buffer = ByteBuffer.allocate(1);
						boolean cond = true;
						String request = "";
						while (cond) {
							int read = channel.read(buffer);
							cond = read != -1 && read != 0;
							buffer.flip();
							while (buffer.hasRemaining()) {
								request += StandardCharsets.UTF_8.decode(buffer).toString();
							}
							buffer.clear();
						}
						System.out.println(channel.socket().getInetAddress() + "\t READABLE\t" + request);
						request += " echoed by server";
						// cambio l'interesse e ci allego il buffer
						key.interestOps(SelectionKey.OP_WRITE).attach(ByteBuffer.wrap(request.getBytes()));
					} else if (key.isWritable()) {
						SocketChannel client = (SocketChannel) key.channel();
						System.out.println(client.socket().getInetAddress() + "\t WRITABLE");
						ByteBuffer output = (ByteBuffer) key.attachment();
						client.write(output);
						output.clear();
						client.close();
					}
				} catch (IOException cancel) {
					System.err.println(cancel.getMessage());
					key.cancel();
					try {
						key.channel().close();
					} catch (IOException close) {
						close.printStackTrace();
					}

				}
			}
		}

	}

}
